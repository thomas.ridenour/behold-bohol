export const sites = {
    chocolatehills : require('./../images/bannerchocolatehills.jpg'),
    lobocriver : require('./../images/tourist2.jpg'),
    virginisland : require('./../images/virgin97.jpg'),
    candijayfalls : require('./../images/tourist4.jpg'),
    cabilao: require('./../images/cabilao1.jpg'),
    alburquerque : require('./../images/banneralbur.jpg'),
    baclayon : require('./../images/baclayon1.jpg'),
    dauis : require('./../images/bannerdauischurch.jpg'),
    loboc : require('./../images/bannerlobocchurch.jpg'),
    loon : require('./../images/bannerloonchurch.jpg'),
    maribojoc : require('./../images/bannermaribojocchurch.jpg'),
    panglao: require('./../images/bannerpanglaochurch.jpg'),
    abatanrivercommunitylifetour: require('./../images/abatanriver1.jpg'),
    alburquerquecalamaymanufacturetourexperience: require('./../images/alburquerquecalamaymanufacturetour1.jpg'),
    alona: require('./../images/banneralona.jpg'),
    andaquinalewhitesandbeach: require('./../images/anda96.jpg'),
    lamanocislandmystictourexperience: require('./../images/lamanoc1.jpg'),
    atvecoadventuretour: require('./../images/atv1.jpg'),
    balaysahumaylearningexperiencetour: require('./../images/balaysahumaylearningexperiencetour1.jpg'),
    balicasagislandescapade: require('./../images/bannerbalicasag.jpg'),
    cabganislandbatwatchingmangrovetour: require('./../images/cabgan1.jpg'),
    cadapdapanriceterraces: require('./../images/cadapdapanriceterraces1.jpg'),
    cambugsayhealinghills: require('./../images/cambugsay1.jpg'),
    cambuhatrivervillageoysterfarm: require('./../images/cambuhatrivervillagetouroysterfarm1.jpg'),
    canawacoldspring: require('./../images/canawacoldspring1.jpg'),
    cantijongcave: require('./../images/cantijongcave1.jpg'),
    canumantadfalls: require('./../images/canumantadfalls1.jpg'),
    clarinancestralhouseexperiencetour: require('./../images/clarinancestralhouseexperiencetour1.jpg'),
    dagookadventuretourexperience: require('./../images/dagookadventuretourexperience1.jpg'),
    dumaluan: require('./../images/bannerdumaluan.jpg'),
    dumogsandbar: require('./../images/dumogsandbar1.jpg'),
    fireflykayakingexperiencetour: require('./../images/fireflykayakingexperiencetour1.jpg'),
    hinagdanancave: require('./../images/bannerhinagdanan.jpg'),
    jagnacalamayindustrytour: require('./../images/jagnacalamayindustrytour1.jpg'),
    loayironsmithtourexperience: require('./../images/loayironsmithtourexperience1.jpg'),
    inangangan: require('./../images/bannerinangangan.jpg'),
    lumayagsandbar: require('./../images/lumayagsandbar1.jpg'),
    maribojocorganicdemofarm: require('./../images/maribojocorganicdemofarm1.jpg'),
    puntacruzwatchtower: require('./../images/bannerpuntacruz.jpg'),
    mountainbiketours: require('./../images/mountainbiketours1.jpg'),
    ondoyslearningfarm: require('./../images/ondoy1.jpg'),
    duroyslearningfarm: require('./../images/duroy1.jpg'),
    pamilacanislanddolphinwhalewatchingtour: require('./../images/bannerpamilacan.jpg'),
    philippinetarsiersanctuary: require('./../images/philippinetarsiersanctuary1.jpg'),
    savimamangroveadventuretour: require('./../images/savimamangroveadventuretour1.jpg'),
    sikatunatreepark: require('./../images/sikatunatree1.jpg'),
    sinandiganmangrovereforestation: require('./../images/sinandiganmangrovereforestation1.jpg'),
    standuppaddlingecoadventuretours: require('./../images/standuppaddlingecoadventuretours1.jpg'),
    boholbiodiversitycomplex: require('./../images/treeplantingforlegacyexperiencetour1.jpg'),
    tribueskayalearningtour: require('./../images/tribueskayalearningtour1.jpg'),
    tubigonloomweavingexperiencetour: require('./../images/tubigonloomweavingexperiencetour1.jpg'),
    ubaystockfarm: require('./../images/ubaystockfarm1.jpg'),
    baclayonchurch : require('./../images/bannerbaclayonchurch.jpg'),
    bilarmanmadeforest : require('./../images/bilar96.jpg'),
    habitatbutterfly : require('./../images/bannerbutterfly.jpg'),
    rivercruiseandfloatingrestaurantinloay : require('./../images/bannerloaycruise.jpg'),
    lobocrivercruiseandfloatingrestaurant : require('./../images/bannerloboccruise.jpg'),
    logaritaspring : require('./../images/logarita1.jpg'),
    //loaybloodcompactsite : require('./../images/sandugoloay1.jpg'),
    bloodcompactcommemorativeshrine: require('./../images/bloodcompact96.jpg'),
    shiphaus : require('./../images/shiphaus1.jpg'),
    sunsetviewfromtagbilaran : require('./../images/sunsetview1.jpg'),
    tarsierconservationinloboc : require('./../images/tarsier1.jpg'),
    mundungsandbar : require('./../images/mundung1.jpg'),
    bagonbanwaisland: require('./../images/bagonbanwa1.jpg'),
    banacon : require('./../images/banacon96.jpg'),
    catangtanganpoint : require('./../images/catang1.jpg'),
    dakit : require('./../images/dakit1.jpg'),
    mantataoisland :require('./../images/mantatao1.jpg'),
    mocabocisland :require('./../images/mocaboc1.jpg'),
    naasug  :require('./../images/naasug1.jpg'),
    popog :require('./../images/popog1.jpg'),
    lintuan :require('./../images/lintuan1.jpg'),
    pahangogfalls :require('./../images/bannerpahangog.jpg'),
    canawacoldspring :require('./../images/bannercanawa.jpg'),
    abatan :require('./../images/abatan1.jpg'),
    cabagnow :require('./../images/cabagnow96.jpg'),
    camugao :require('./../images/camugao1.jpg'),
    danicop :require('./../images/danicop1.jpg'),
    magaso :require('./../images/magaso1.jpg'),
    kulangkulang :require('./../images/kulang1.jpg'),
    lonoy :require('./../images/lonoy1.jpg'),
    binaliw :require('./../images/binaliw1.jpg'),
    inambacanspring :require('./../images/inambacan1.jpg'),
    ingkumhan :require('./../images/banneringkumhan.jpg'),
    kilabkilabfalls :require('./../images/kilabkilab1.jpg'),
    kinahuganfalls :require('./../images/kinahugan1.jpg'),
    loctob :require('./../images/loctob1.jpg'),
    malingin :require('./../images/malingin1.jpg'),
    mandahunogcave :require('./../images/mandahunog96.jpg'),
    pangasfalls :require('./../images/pangas1.jpg'),
    tubigloon :require('./../images/bannertubigloon.jpg'),
    damagan :require('./../images/bannerdamagan.jpg'),
    kamira :require('./../images/kamira1.jpg'),
    chap :require('./../images/chap1.jpg'),
    danaoadventurepark :require('./../images/dap1.jpg'),
    haboog :require('./../images/haboog1.jpg'),
    bigcross :require('./../images/bigcross1.jpg'),
    morninghills :require('./../images/morninghills1.jpg'),
    seaofclouds: require('./../images/seaofclouds96.jpg'),
     panglaobeach: require('./../images/bingag1.jpg'),
     amarelabeach: require('./../images/amarela1.jpg'),
     sevillahangingbridge: require('./../images/bannersevilla.jpg'),
     doljobeach: require('./../images/doljobeach.jpg'),
     bikinibeach: require('./../images/bannerbikini.jpg'),
     momobeach: require('./../images/bannermomo.jpg'),
     sanisidrobeach: require('./../images/sanisidro1.jpg'),
     bagobobeach: require('./../images/bannerbagobo.jpg'),
     danaobeach: require('./../images/danao1.jpg'),
     daobeach: require('./../images/dao1.jpg'),
     santafebeach: require('./../images/santafe1.jpg'),
     layabeach: require('./../images/laya96.jpg'),
     canubabeach: require('./../images/bannercanuba.jpg'),
     pisikfalls: require('./../images/pisik1.jpg'),
     carlospgarcia : require('./../images/bannercpg.jpg'),
   boholnationalmuseum : require('./../images/bannermuseum.jpg'),
   //provincialcapitolbuilding : require('./../images/capitol.jpg'),
   cathedralsaintjoseph : require('./../images/bannercathedral.jpg'),
   sitioubos : require('./../images/bannersitio.jpg'),
   mabawreef : require('./../images/bannermabaw.jpg'),
   balicasagdive :require('./../images/bannerbalicasag.jpg'),
    danajon :require('./../images/bannerdanajon.jpg'),
    cabilaodive :require('./../images/bannercabilao.jpg'),
    pamilacandive :require('./../images/bannerpamilacan.jpg'),
    shipwreck :require('./../images/bannershipwreck.jpg'),
    gardeneels :require('./../images/bannergardeneels.jpg'),
    snakeisland :require('./../images/bannnerseasnake.jpg'),
    doljopoint :require('./../images/bannerdoljo.jpg'),
    bienunidodive :require('./../images/bien96.jpg'),
    tarsierconservationareabilar :require('./../images/bilartarsier.jpg'),
    pandanonisland :require('./../images/bannerpandanon.jpg'),
    kabantianfalls :require('./../images/bannerkabantian.jpg'),
    piongfalls :require('./../images/bannerpiong.jpg'),
    kawasanfalls :require('./../images/bannerkawasan.jpg'),
    panasfalls :require('./../images/bannerpanas.jpg'),
    badiangspring :require('./../images/bannerbadiang.jpg'),
    anislagspring :require('./../images/banneranislag.jpg'),
    tambukofalls :require('./../images/bannertambuko.jpg'),
    pugowspring :require('./../images/bannertabuan.jpg'),
    kawafalls :require('./../images/bannerkawa.jpg'),
    tinugdanfalls :require('./../images/bannertinugdan.jpg'),
    niluksuanfalls :require('./../images/bannerniluksuan.jpg'),
    niafalls :require('./../images/bannernia.jpg'),
    maambong :require('./../images/bannermaambong.jpg'),
    combento :require('./../images/bannercombento.jpg'),
    ipofalls :require('./../images/banneripo.jpg'),
    magsijafalls :require('./../images/bannermagsija.jpg'),
    langgamcave :require('./../images/bannerlangam.jpg'),
    evacave :require('./../images/bannereva.jpg'),
    bitoon :require('./../images/bannerbitoon.jpg'),
    alicia :require('./../images/banneralicia.jpg'),
    jardin :require('./../images/bannerjardin.jpg'),

};


export const culture = {
  saulog : require('./../images/bannersaulog.jpg'),
  agbunan: require('./../images/banneragbunan.jpg'),
  bolibongkingking: require('./../images/bannerboling.jpg'),
  hudyakasapanglaofestival: require('./../images/bannerhudyaka.jpg'),
  calamayfestival: require('./../images/bannercalamay.jpg'),
  kasadyaansanapo: require('./../images/bannerkasadyaan.jpg'),
  katigbawan: require('./../images/bannerkatigbawan.jpg'),
  panaadsaloboc: require('./../images/bannerpanaad.jpg'),
  pandayanfestival: require('./../images/bannerpandayan.jpg'),
  sandugo: require('./../images/bannersandugo.jpg'),
  sidlakasilak: require('./../images/bannerkasilak.jpg'),
  sinugboanfestival: require('./../images/bannersinugbuan.jpg'),
  sinuogestokada: require('./../images/sinuog0.jpg'),
  suroysamusikero: require('./../images/suroysamusikero1.jpg'),
  ubifestival: require('./../images/bannerubi.jpg'),
  sambatmascarayregattafestival: require('./../images/bannersambat.jpg'),
  binolanon: require('./../images/phrases.jpg')
};

export const menu = {
  sites: require('./../images/visit2.jpg'),
  dining: require('./../images/eat2.jpg'),
  culture: require('./../images/culture96.jpg'),
  accomodation: require('./../images/stay2.jpg'),
//  wellness: require('./../images/MenuWellness.png'),
  shopping: require('./../images/wellness.jpg'),
  banking: require('./../images/bank.jpg'),
  touring: require('./../images/cttp.jpg'),
  public2: require('./../images/govmansion.jpg'),
  phone: require('./../images/globe2.jpg')
};

export const saulog= [
  require('./../images/saulog2.jpg')
];

export const magsijafalls = [
  require('./../images/magsija.jpg'),
  require('./../images/magsija1.jpg'),
  require('./../images/magsija2.jpg'),
  require('./../images/magsija3.jpg'),
  require('./../images/magsija4.jpg'),
  
  
];
export const jardin = [
  require('./../images/jardin.jpg'),
  require('./../images/jardin1.jpg'),
  require('./../images/jardin2.jpg'),
  require('./../images/jardin3.jpg'),
  require('./../images/jardin4.jpg'),
  require('./../images/jardin5.jpg'),
  
  
];

export const evacave = [
  require('./../images/eva.jpg'),
  require('./../images/eva1.jpg'),
  require('./../images/eva2.jpg'),
  require('./../images/eva3.jpg'),

  
  
];

export const combento = [
  require('./../images/combento.jpg'),
  require('./../images/combento1.jpg'),
  require('./../images/combento2.jpg'),
  require('./../images/combento3.jpg'),
  require('./../images/combento4.jpg'),
  require('./../images/combento5.jpg'),
  require('./../images/combento6.jpg'),
  
  
];
export const alicia = [
   require('./../images/alicia.jpg'),
  require('./../images/alicia1.jpg'),
  require('./../images/alicia2.jpg'),
  require('./../images/alicia3.jpg'),
  require('./../images/alicia4.jpg'),
  require('./../images/alicia5.jpg'),
  require('./../images/alicia6.jpg'),
  require('./../images/alicia7.jpg'),
  require('./../images/alicia8.jpg'),
  require('./../images/alicia9.jpg'),
  
  
];


export const bitoon = [

  require('./../images/bitoon1.jpg'),
  require('./../images/bitoon2.jpg'),
  require('./../images/bitoon3.jpg'),
  require('./../images/bitoon4.jpg'),
  require('./../images/bitoon5.jpg'),
  require('./../images/bitoon6.jpg'),
  require('./../images/bitoon7.jpg'),
  require('./../images/bitoon8.jpg'),
  require('./../images/bitoon9.jpg'),
  
  
];


export const langgamcave = [
  require('./../images/langgam.jpg'),
  require('./../images/langgam1.jpg'),
  require('./../images/langgam2.jpg'),
  require('./../images/langgam3.jpg'),
  require('./../images/langgam4.jpg'),
  require('./../images/langgam5.jpg'),
  require('./../images/langgam6.jpg'),
  require('./../images/langgam7.jpg'),
  require('./../images/langgam8.jpg'),
  require('./../images/langgam9.jpg'),
  require('./../images/langgam10.jpg'),
  require('./../images/langgam11.jpg'),
  require('./../images/langgam12.jpg'),
  
  
];
export const maambong = [
  require('./../images/maambong.jpg'),
  require('./../images/maambong1.jpg'),
  require('./../images/maambong2.jpg'),
  require('./../images/maambong3.jpg'),
  
  
];

export const maambong = [
  require('./../images/ipo.jpg'),
  require('./../images/ipo1.jpg'),
  
  
];


export const niafalls = [
  require('./../images/nia1.jpg'),
  require('./../images/nia2.jpg'),
  require('./../images/nia3.jpg'),
  require('./../images/nia4.jpg'),
  require('./../images/nia5.jpg'),
   

];
export const niluksuanfalls = [
  require('./../images/niluksuan.jpg'),
  require('./../images/niluksuan1.jpg'),
  require('./../images/niluksuan2.jpg'),
   

];

export const tinugdanfalls = [
  require('./../images/tinugdan1.jpg'),
  require('./../images/tinugdan2.jpg'),
  require('./../images/tinugdan3.jpg'),
   require('./../images/tinugdan4.jpg'),
  
 

];
export const kawafalls = [
  require('./../images/kawa.jpg'),
  require('./../images/kawa1.jpg'),
  require('./../images/kawa2.jpg'),
  
 

];

export const pugowspring = [
  require('./../images/tabuan.jpg'),
  require('./../images/tabuan1.jpg'),
  require('./../images/tabuan2.jpg'),
  
 

];

export const tambukofalls = [
  require('./../images/tambuko1.jpg'),
  require('./../images/tambuko2.jpg'),
  require('./../images/tambuko3.jpg'),
  require('./../images/tambuko4.jpg'),
 

];

export const pandanonisland = [
  require('./../images/pandanon1.jpg'),
  require('./../images/pandanon2.jpg'),
  require('./../images/pandanon3.jpg'),
  require('./../images/pandanon4.jpg'),
  require('./../images/pandanon5.jpg'),
  require('./../images/pandanon6.jpg'),
  require('./../images/pandanon7.jpg'),

];

export const piongfalls = [
  require('./../images/piong1.jpg'),
  require('./../images/piong2.jpg'),
  require('./../images/piong3.jpg'),
  require('./../images/piong4.jpg'),


];

export const badiangspring = [
  require('./../images/badiang1.jpg'),
  require('./../images/badiang2.jpg'),
  require('./../images/badiang3.jpg'),
  require('./../images/badiang4.jpg'),


];
export const anislagspring = [
  require('./../images/badiang1.jpg'),
  require('./../images/badiang2.jpg'),



];


export const kawasanfalls = [
  require('./../images/kawasanfalls1.jpg'),
  require('./../images/kawasanfalls2.jpg'),
  require('./../images/kawasanfalls3.jpg'),
  require('./../images/kawasanfalls4.jpg'),
  require('./../images/kawasanfalls5.jpg'),


];








export const kabantianfalls = [
  require('./../images/kabantian1.jpg'),
  require('./../images/kabantian2.jpg'),
  require('./../images/kabantian3.jpg'),
  require('./../images/kabantian4.jpg'),
  require('./../images/kabantian5.jpg'),
  require('./../images/kabantian6.jpg'),
  require('./../images/kabantian7.jpg'),
  require('./../images/kabantian8.jpg'),
  require('./../images/kabantian9.jpg'),

];




export const virginisland = [
  //require('./../images/virgin2.jpg'),
  //require('./../images/virgin3.jpg'),
  //require('./../images/virgin4.jpg'),
  //require('./../images/virgin96.jpg'),
  require('./../images/virgin5.jpg'),
  //require('./../images/virgin6.jpg'),
  require('./../images/virgin7.jpg'),
  require('./../images/virgin8.jpg'),
  require('./../images/virgin9.jpg'),
];


export const alona = [
  require('./../images/alona2.jpg'),
  require('./../images/alona4.jpg'),
  require('./../images/alona5.jpg'),
  //require('./../images/alona6.jpg'),
  require('./../images/alona7.jpg'),
  require('./../images/alona8.jpg'),
  require('./../images/alona9.jpg'),
  require('./../images/alona10.jpg'),
  require('./../images/alona11.jpg'),
  require('./../images/alona12.jpg'),

];



export const dumaluan = [
//require('./../images/dumaluan2.jpg'),
//require('./../images/dumaluan3.jpg'),
//require('./../images/dumaluan5.jpg'),
//require('./../images/dumaluan6.jpg'),
require('./../images/beachdumaluan1.jpg'),
require('./../images/beachdumaluan2.jpg'),
require('./../images/beachdumaluan3.jpg'),
require('./../images/beachdumaluan4.jpg'),
require('./../images/beachdumaluan5.jpg'),
require('./../images/dumaluan8.jpg'),
//require('./../images/dumaluan9.jpg'),
//require('./../images/dumaluan10.jpg'),
require('./../images/dumaluan11.jpg'),
require('./../images/dumaluan7.jpg'),
//require('./../images/dumaluan12.jpg'),
];

export const cabilao = [
  require('./../images/cabilao2.jpg'),
  require('./../images/cabilao3.jpg'),
  require('./../images/cabilao4.jpg'),
  //require('./../images/cabilao5.jpg'),
  require('./../images/cabilao6.jpg'),
  require('./../images/cabilao7.jpg'),
  require('./../images/cabilao8.jpg'),

];

export const alburquerque = [
  require('./../images/alburquerquechronicle.jpg'),
];


export const baclayon = [
  require('./../images/baclayon.jpg'),
];

export const dauis = [
  require('./../images/dauischurch.jpg'),
];

export const loboc = [
  require('./../images/lobocchurch.jpg'),
];

export const loon = [
  require('./../images/loonchurch.jpg'),
];

export const maribojocorganicdemofarm = [
  require('./../images/maribojocfarm2.jpg'),
  require('./../images/maribojocfarm3.jpg'),
  require('./../images/maribojocfarm4.jpg'),
  require('./../images/maribojocfarm5.jpg'),

];

export const maribojoc = [
  require('./../images/maribojocchurch.jpg'),
];

export const panglao = [
  require('./../images/panglaochurch1.jpg'),
];

export const agbunan = [
  require('./../images/agbunan2.jpg'),
];

export const bolibongkingking = [
  require('./../images/bolibong.jpg'),
];

export const calamayfestival = [
  require('./../images/calamay2.jpg'),
];

export const hudyakasapanglaofestival = [
  require('./../images/hudyaka2.jpg'),
];

 export const kasadyaansanapo = [
  require('./../images/kasadya.jpg'),
  require('./../images/kasadya1.jpg'),
 ];

export const katigbawan = [
  require('./../images/katigbawan2.jpg'),
];

 export const panaadsaloboc = [
   require('./../images/panaad2.jpg'),
 ];

export const pandayanfestival = [
  require('./../images/pandayan2.jpg'),
];

export const sambatmascarayregattafestival = [
  require('./../images/sambat.jpg'),
];

export const sandugo = [
  require('./../images/sandugo2.jpg'),
];

export const sidlakasilak = [
  require('./../images/kasilak.jpg'),
];

export const sinugboanfestival = [
  require('./../images/sinugbuan2.jpg'),
];

export const sinuogestokada = [
  require('./../images/sinuog.jpg'),
];

 export const suroysamusikero = [
   require('./../images/surokmusikero.jpg'),
];

export const ubifestival = [
require('./../images/ubifestival2.jpg'),
];

export const abatanrivercommunitylifetour = [
  require('./../images/abatan2.jpg'),
  require('./../images/abatan3.jpg'),
];

export const  alburquerquecalamaymanufacturetourexperience = [
  require('./../images/loaycalamay2.jpg'),
  require('./../images/loaycalamay3.jpg'),
  require('./../images/loaycalamay4.jpg'),

];

export const andaquinalewhitesandbeach = [
require('./../images/quinale17.jpg'),
require('./../images/quinale8.jpg'),
require('./../images/quinale9.jpg'),
require('./../images/quinale10.jpg'),
require('./../images/quinale11.jpg'),
require('./../images/quinale12.jpg'),
require('./../images/quinale13.jpg'),
require('./../images/quinale14.jpg'),
require('./../images/quinale15.jpg'),
require('./../images/quinale16.jpg'),
//require('./../images/quinale17.jpg'),
  //require('./../images/quinale2.jpg'),
  //require('./../images/quinale3.jpg'),
  //require('./../images/quinale4.jpg'),
  //require('./../images/quinale5.jpg'),
  //require('./../images/quinale6.jpg'),
];

 export const lamanocislandmystictourexperience = [
  require('./../images/lamanoc2.jpg'),
  require('./../images/lamanoc3.jpg'),
  require('./../images/lamanoc4.jpg'),
  require('./../images/lamanoc5.jpg'),
 ];

export const atvecoadventuretour = [
  require('./../images/atv2.jpg'),
  require('./../images/atv3.jpg'),
  require('./../images/atv4.jpg'),
  require('./../images/atv5.jpg'),
];

export const balaysahumaylearningexperiencetour = [
  require('./../images/humay2.jpg'),
  require('./../images/humay3.jpg'),
  require('./../images/humay4.jpg'),
  require('./../images/humay5.jpg'),
];

export const balicasagislandescapade = [
  //require('./../images/balicasag2.jpg'),
  //require('./../images/balicasag3.jpg'),
  require('./../images/balicasagisland2.jpg'),
  require('./../images/balicasagisland.jpg'),
  require('./../images/balicasag4.jpg'),
  //require('./../images/balicasag5.jpg'),
  //require('./../images/balicasag6.jpg'),
  require('./../images/balicasag7.jpg'),
  require('./../images/balicasag8.jpg'),
  require('./../images/balicasag9.jpg'),
  require('./../images/balicasag10.jpg'),
  require('./../images/balicasag11.jpg'),
   require('./../images/balicasag12.jpg'),
];

export const cabganislandbatwatchingmangrovetour = [
  require('./../images/cabgan2.jpg'),
  require('./../images/cabgan3.jpg'),
  require('./../images/cabgan4.jpg'),
  require('./../images/cabgan5.jpg'),
];

export const cadapdapanriceterraces = [
  require('./../images/rice2.jpg'),
  require('./../images/rice3.jpg'),
  require('./../images/rice4.jpg'),
  require('./../images/rice5.jpg'),
  require('./../images/rice6.jpg'),
  require('./../images/rice7.jpg'),
  require('./../images/rice8.jpg'),
  require('./../images/rice9.jpg'),
];

export const cambuhatrivervillageoysterfarm = [
  require('./../images/cambuhat2.jpg'),
  require('./../images/cambuhat3.jpg'),
  require('./../images/cambuhat4.jpg'),
];

export const cambugsayhealinghills = [
  require('./../images/cambugsay2.jpg'),
  require('./../images/cambugsay3.jpg'),
  require('./../images/cambugsay4.jpg'),
  require('./../images/cambugsay5.jpg'),
];

export const canumantadfalls = [
  //require('./../images/canumantad2.jpg'),
  require('./../images/canumantad3.jpg'),
  //require('./../images/canumantad4.jpg'),
  //require('./../images/canumantad5.jpg'),
  require('./../images/canumantad6.jpg'),
  require('./../images/canumantad7.jpg'),
  require('./../images/canumantad8.jpg'),
  require('./../images/canumantad9.jpg'),
  require('./../images/canumantad10.jpg'),
  require('./../images/canumantad11.jpg'),
  require('./../images/canumantad12.jpg'),

];

export const canawacoldspring = [
  require('./../images/canawa2.jpg'),
  //require('./../images/canawa3.jpg'),
  require('./../images/canawa4.jpg'),
  //require('./../images/canawa5.jpg'),
  require('./../images/canawa6.jpg'),
  require('./../images/canawa7.jpg'),
  require('./../images/canawa8.jpg'),
  require('./../images/canawa9.jpg'),
  require('./../images/canawa10.jpg'),
];

export const cantijongcave = [
  require('./../images/cantijong2.jpg'),
  require('./../images/cantijong3.jpg'),
  require('./../images/cantijong4.jpg'),
];

export const clarinancestralhouseexperiencetour = [
  require('./../images/ancestral2.jpg'),
  require('./../images/ancestral3.jpg'),
  require('./../images/ancestral4.jpg'),
];

export const dagookadventuretourexperience = [
  require('./../images/dagook6.jpg'),
  require('./../images/dagook7.jpg'),
  require('./../images/dagook8.jpg'),
  require('./../images/dagook9.jpg'),
  require('./../images/dagook10.jpg'),
  require('./../images/dagook11.jpg'),
  require('./../images/dagook13.jpg'),

];

export const dumogsandbar = [
  require('./../images/dumog2.jpg'),
  require('./../images/dumog3.jpg'),
  require('./../images/dumog4.jpg'),
  require('./../images/dumog5.jpg'),
  require('./../images/dumog6.jpg'),
  require('./../images/dumog7.jpg'),
  require('./../images/dumog8.jpg'),
];

export const duroyslearningfarm = [
  require('./../images/duroy2.jpg'),
];

export const hinagdanancave = [
require('./../images/hinagdanancave5.jpg'),
require('./../images/hinagdanancave6.jpg'),
require('./../images/hinagdanancave7.jpg'),
require('./../images/hinagdanancave8.jpg'),
require('./../images/hinagdanancave9.jpg'),
require('./../images/hinagdanancave10.jpg'),
require('./../images/hinagdanancave11.jpg'),
require('./../images/hinagdanancave12.jpg'),
require('./../images/hinagdanancave13.jpg'),
];

export const inangangan = [
  require('./../images/inangangan2.jpg'),
  require('./../images/inangangan3.jpg'),
  require('./../images/stairway.jpg'),
  //require('./../images/inangangan4.jpg'),
  //require('./../images/inangangan5.jpg'),
];

export const jagnacalamayindustrytour = [
  require('./../images/jagna2.jpg'),
  require('./../images/jagna3.jpg'),
  require('./../images/jagna4.jpg'),
  require('./../images/jagna5.jpg'),
];

export const loayironsmithtourexperience = [
  require('./../images/loayiron2.jpg'),
  require('./../images/loayiron3.jpg'),
  require('./../images/loayiron4.jpg'),
  require('./../images/loayiron5.jpg'),
];

export const lumayagsandbar = [
  require('./../images/lumayag2.jpg'),
  require('./../images/lumayag3.jpg'),
  require('./../images/lumayag4.jpg'),
  //require('./../images/lumayag5.jpg'),
 //require('./../images/lumayag6.jpg'),
];

export const savimamangroveadventuretour = [
  require('./../images/savima2.jpg'),
  require('./../images/savima3.jpg'),
  require('./../images/savima4.jpg'),
  require('./../images/savima5.jpg'),
];

export const fireflykayakingexperiencetour = [
  require('./../images/kayaking2.jpg'),
  require('./../images/kayaking3.jpg'),
  require('./../images/kayaking4.jpg'),
  require('./../images/kayaking5.jpg'),
];

export const mountainbiketours = [
  require('./../images/mountainbike2.jpg'),
  require('./../images/mountainbike3.jpg'),
  require('./../images/mountainbike4.jpg'),
  require('./../images/mountainbike5.jpg'),
];

export const ondoyslearningfarm = [
  require('./../images/ondoy2.jpg'),
  require('./../images/ondoy3.jpg'),
];

export const pamilacanislanddolphinwhalewatchingtour = [
  //require('./../images/pamilacan2.jpg'),
  //require('./../images/pamilacan3.jpg'),
  //require('./../images/pamilacan4.jpg'),
  //require('./../images/pamilacan5.jpg'),
  //require('./../images/pamilacan6.jpg'),
  //require('./../images/pamilacan7.jpg'),
  require('./../images/pamilacan8.jpg'),
  require('./../images/pamilacan9.jpg'),
  require('./../images/pamilacan10.jpg'),
  require('./../images/pamilacan11.jpg'),
  require('./../images/pamilacan12.jpg'),
  require('./../images/pamilacan13.jpg'),
  require('./../images/pamilacan14.jpg'),

];

export const puntacruzwatchtower = [
  //require('./../images/puntacruz2.jpg'),
  //require('./../images/puntacruz3.jpg'),
  require('./../images/puntacruz.jpg'),
  require('./../images/puntacruz4.jpg'),
  require('./../images/puntacruz6.jpg'),
  require('./../images/puntacruz7.jpg'),
  require('./../images/puntacruz8.jpg'),
  require('./../images/puntacruz9.jpg'),
];

export const sikatunatreepark = [
  require('./../images/treepark2.jpg'),
  require('./../images/treepark3.jpg'),
  require('./../images/treepark4.jpg'),
  require('./../images/treepark5.jpg'),
];

export const sinandiganmangrovereforestation = [
  require('./../images/sinandigan2.jpg'),
  require('./../images/sinandigan3.jpg'),
  require('./../images/sinandigan4.jpg'),
  require('./../images/sinandigan5.jpg'),
];

export const standuppaddlingecoadventuretours = [
  require('./../images/standup2.jpg'),
  require('./../images/standup3.jpg'),
  require('./../images/standup4.jpg'),
  require('./../images/standup5.jpg'),
];

export const philippinetarsiersanctuary = [
  require('./../images/corellatarsier2.jpg'),
  require('./../images/corellatarsier3.jpg'),
  require('./../images/corellatarsier4.jpg'),
];

export const boholbiodiversitycomplex = [
  require('./../images/diversity2.jpg'),
  require('./../images/diversity3.jpg'),
  require('./../images/diversity4.jpg'),
  require('./../images/diversity5.jpg'),
];

export const tribueskayalearningtour = [
  require('./../images/eskaya2.jpg'),
  require('./../images/eskaya3.jpg'),
  require('./../images/eskaya4.jpg'),
];

export const tubigonloomweavingexperiencetour = [
  require('./../images/loom2.jpg'),
  require('./../images/loom3.jpg'),
  require('./../images/loom4.jpg'),
  require('./../images/loom5.jpg'),
];

export const ubaystockfarm = [
  require('./../images/stockfarm2.jpg'),
  require('./../images/stockfarm3.jpg'),
  require('./../images/stockfarm4.jpg'),
  require('./../images/stockfarm5.jpg'),
];

export const baclayonchurch = [
  require('./../images/baclayonchurch1.jpg'),
];

export const bilarmanmadeforest = [
  //require('./../images/bilar2.jpg'),
  //require('./../images/bilar3.jpg'),
  //require('./../images/bilar5.jpg'),
  //require('./../images/bilar6.jpg'),
  require('./../images/bilar11.jpg'),
  require('./../images/bilar8.jpg'),
  require('./../images/bilar9.jpg'),
  require('./../images/bilar10.jpg'),
];

export const habitatbutterfly = [
 // require('./../images/butterfly2.jpg'),
  require('./../images/butterfly3.jpg'),
//  require('./../images/butterfly4.jpg'),
//  require('./../images/butterfly5.jpg'),
];

export const chocolatehills = [
  //require('./../images/chocolate2.jpg'),
  //require('./../images/chocolate3.jpg'),
  require('./../images/chocolate4.jpg'),
  require('./../images/chocolate5.jpg'),
  //require('./../images/chocolate6.jpg'),
  require('./../images/chocolate7.jpg'),

];

export const rivercruiseandfloatingrestaurantinloay = [
  //require('./../images/loayrivercruise2.jpg'),
  //require('./../images/loayrivercruise3.jpg'),
  //require('./../images/loayrivercruise4.jpg'),
  //require('./../images/loayrivercruise5.jpg'),
  //require('./../images/loayrivercruise6.jpg'),
  require('./../images/rioverde.jpg'),
  require('./../images/rioverde2.jpg'),
  require('./../images/rioverde3.jpg'),
  require('./../images/rioverde4.jpg'),
];

export const lobocrivercruiseandfloatingrestaurant = [
  require('./../images/lobocriver4.jpg'),
  //require('./../images/lobocriver5.jpg'),
];

export const logaritaspring = [
  require('./../images/logarita2.jpg'),
  require('./../images/logarita3.jpg'),
  require('./../images/logarita4.jpg'),
  require('./../images/logarita5.jpg'),
  require('./../images/logarita6.jpg'),
  require('./../images/logarita7.jpg'),
];

export const loaybloodcompactsite = [
  //require('./../images/loaybloodcompact2.jpg'),
  require('./../images/loaybloodcompact3.jpg'),
  //require('./../images/loaybloodcompact4.jpg'),
];

export const bloodcompactcommemorativeshrine = [
  //require('./../images/tagbilaranbloodcompact2.jpg'),
  require('./../images/tagbilaranbloodcompact3.jpg'),
  require('./../images/tagbilaranbloodcompact4.jpg'),
];

export const shiphaus = [
  require('./../images/shiphaus.jpg'),
];

export const sunsetviewfromtagbilaran = [
  require('./../images/sunsetview2.jpg'),
//  require('./../images/sunsetview3.jpg'),
//  require('./../images/sunsetview4.jpg'),
];

export const mundungsandbar = [
  require('./../images/mundung2.jpg'),
  require('./../images/mundung3.jpg'),
  require('./../images/mundung4.jpg'),
  require('./../images/mundung5.jpg'),
  require('./../images/mundung6.jpg'),
  require('./../images/mundung7.jpg'),
  require('./../images/mundung8.jpg'),
  require('./../images/mundung9.jpg'),
  require('./../images/mundung10.jpg'),
];

export const bagonbanwaisland = [
  require('./../images/bagonbanwa2.jpg'),
  require('./../images/bagonbanwa3.jpg'),
  //require('./../images/bagonbanwa4.jpg'),
  require('./../images/bagonbanwa5.jpg'),
];

export const pisikfalls = [
  require('./../images/pisik2.jpg'),
  require('./../images/pisik3.jpg'),
  require('./../images/pisik4.jpg'),
  require('./../images/pisik5.jpg'),
  require('./../images/pisik6.jpg'),
];


export const panasfalls = [
  require('./../images/panas1.jpg'),
  require('./../images/panas2.jpg'),
  require('./../images/panas3.jpg'),
  require('./../images/panas4.jpg'),
  require('./../images/panas5.jpg'),
];

export const banacon = [
  //require('./../images/banacon2.jpg'),
  //require('./../images/banacon3.jpg'),
  //require('./../images/banacon4.jpg'),
  require('./../images/banacon5.jpg'),
  require('./../images/banacon6.jpg'),
  require('./../images/banacon7.jpg'),
  //require('./../images/banacon5.jpg'),
];

export const catangtanganpoint = [
  require('./../images/catang2.jpg'),
  require('./../images/catang3.jpg'),
  require('./../images/catang4.jpg'),
  require('./../images/catang5.jpg'),

];

export const dakit = [
  require('./../images/dakit2.jpg'),
  require('./../images/dakit3.jpg'),
  require('./../images/dakit4.jpg'),
];


export const mantataoisland = [
  require('./../images/mantatao2.jpg'),
  require('./../images/mantatao3.jpg'),
  //require('./../images/mantatao4.jpg'),
  //require('./../images/mantatao5.jpg'),
];

export const mocabocisland = [
  require('./../images/mocaboc2.jpg'),
  require('./../images/mocaboc3.jpg'),
  require('./../images/mocaboc4.jpg'),
  require('./../images/mocaboc5.jpg'),
  require('./../images/mocaboc6.jpg'),
];

export const naasug = [
  require('./../images/naasug2.jpg'),
  require('./../images/naasug3.jpg'),
];

export const popog = [
  require('./../images/popog2.jpg'),
  //require('./../images/popog3.jpg'),
  require('./../images/popog4.jpg'),
  require('./../images/popog5.jpg'),

];

export const lintuan = [
  require('./../images/lintuan2.jpg'),
  //require('./../images/lintuan3.jpg'),
  //require('./../images/lintuan4.jpg'),
  require('./../images/lintuan5.jpg'),
  require('./../images/lintuan6.jpg'),
  require('./../images/lintuan7.jpg'),
];

export const pahangogfalls = [
  //require('./../images/twin2.jpg'),
  //require('./../images/twin3.jpg'),
  //require('./../images/twin5.jpg'),
  require('./../images/twin6.jpg'),
  require('./../images/twin7.jpg'),
  require('./../images/twin8.jpg'),
  require('./../images/twin9.jpg'),
  require('./../images/twin10.jpg'),
];

export const abatan = [
  require('./../images/abatanriver2.jpg'),
  require('./../images/abatanriver3.jpg'),
  require('./../images/abatanriver4.jpg'),
  require('./../images/abatanriver5.jpg'),
  //require('./../images/abatanriver5.jpg'),
];

export const cabagnow = [
  //require('./../images/cabagnow2.jpg'),
  //require('./../images/cabagnow3.jpg'),
  //require('./../images/cabagnow4.jpg'),
  //require('./../images/cabagnow5.jpg'),
  require('./../images/cabagnow6.jpg'),
  //require('./../images/cabagnow7.jpg'),
  require('./../images/cabagnow8.jpg'),
  require('./../images/cabagnow9.jpg'),
  require('./../images/cabagnow10.jpg'),
  require('./../images/cabagnow11.jpg'),
  require('./../images/cabagnow12.jpg'),
  require('./../images/cabagnow13.jpg'),
  require('./../images/cabagnow14.jpg'),
  require('./../images/cabagnow15.jpg'),



];

export const camugao = [
  //require('./../images/camugao2.jpg'),
  require('./../images/camugao3.jpg'),
  //require('./../images/camugao4.jpg'),
  //require('./../images/camugao5.jpg'),
  require('./../images/camugao6.jpg'),
  require('./../images/camugao7.jpg'),
];

export const danicop = [
  //require('./../images/danicop2.jpg'),
  require('./../images/danicop3.jpg'),
  require('./../images/danicop4.jpg'),
  require('./../images/danicop5.jpg'),
  require('./../images/danicop6.jpg'),
];

export const magaso = [
  //require('./../images/magaso2.jpg'),
  //require('./../images/magaso3.jpg'),
  //require('./../images/magaso4.jpg'),
  //require('./../images/magaso5.jpg'),
  require('./../images/magaso6.jpg'),
  require('./../images/magaso7.jpg'),
  require('./../images/magaso8.jpg'),
  require('./../images/magaso9.jpg'),
];

export const kulangkulang = [
  require('./../images/kulang2.jpg'),
  require('./../images/kulang3.jpg'),
  require('./../images/kulang4.jpg'),
  require('./../images/kulang5.jpg'),
  require('./../images/kulang6.jpg'),
  require('./../images/kulang7.jpg'),
];

export const lonoy = [
  //require('./../images/lonoy2.jpg'),
  //require('./../images/lonoy3.jpg'),
  require('./../images/lonoy4.jpg'),
  require('./../images/lonoy5.jpg'),
  require('./../images/lonoy6.jpg'),
  require('./../images/lonoy7.jpg'),
];

export const binaliw = [
  require('./../images/binaliw2.jpg'),
  //require('./../images/binaliw3.jpg'),
  //require('./../images/binaliw4.jpg'),
  //require('./../images/binaliw5.jpg'),
  require('./../images/binaliw3.jpg'),
  require('./../images/binaliw4.jpg'),
  require('./../images/binaliw5.jpg'),
  require('./../images/binaliw6.jpg'),
  require('./../images/binaliw7.jpg'),
];

export const inambacanspring = [
  //require('./../images/inambacan2.jpg'),
  require('./../images/inambacan3.jpg'),
  require('./../images/inambacan4.jpg'),
  //require('./../images/inambacan4.jpg'),
  require('./../images/inambacan5.jpg'),
  //require('./../images/inambacan6.jpg'),
];

export const ingkumhan = [
  require('./../images/ingkumhan2.jpg'),
  require('./../images/ingkumhan3.jpg'),
  require('./../images/ingkumhan4.jpg'),
  require('./../images/ingkumhan5.jpg'),
];

export const kilabkilabfalls = [
  require('./../images/kilabkilab2.jpg'),
  require('./../images/kilabkilab3.jpg'),
  require('./../images/kilabkilab4.jpg'),
  require('./../images/kilabkilab5.jpg'),
  require('./../images/kilabkilab6.jpg'),
  require('./../images/kilabkilab7.jpg'),
  require('./../images/kilabkilab8.jpg'),
  require('./../images/kilabkilab9.jpg'),
  require('./../images/kilabkilab10.jpg'),
  require('./../images/kilabkilab11.jpg'),
  require('./../images/kilabkilab12.jpg'),
];

export const kinahuganfalls = [
  //require('./../images/kinahugan2.jpg'),
  require('./../images/kinahugan3.jpg'),
  require('./../images/kinahugan4.jpg'),
  require('./../images/kinahugan5.jpg'),
  require('./../images/kinahugan6.jpg'),
  require('./../images/kinahugan7.jpg'),
  require('./../images/kinahugan8.jpg'),
  require('./../images/kinahugan9.jpg'),
  require('./../images/kinahugan10.jpg'),
  require('./../images/kinahugan11.jpg'),
];

export const loctob = [
  require('./../images/loctob2.jpg'),
  require('./../images/loctob3.jpg'),
  require('./../images/loctob4.jpg'),
  require('./../images/loctob5.jpg'),
  require('./../images/loctob6.jpg'),
];

export const malingin = [
  require('./../images/malingin2.jpg'),
  require('./../images/malingin3.jpg'),
  require('./../images/malingin4.jpg'),
  require('./../images/malingin5.jpg'),
  require('./../images/malingin6.jpg'),

];


export const mandahunogcave = [
  //require('./../images/mandahunog2.jpg'),
  //require('./../images/mandahunog3.jpg'),
  //require('./../images/mandahunog4.jpg'),
 // require('./../images/mandahunog5.jpg'),
  require('./../images/mandahunog8.jpg'),
  require('./../images/mandahunog7.jpg'),
  require('./../images/mandahunog9.jpg'),
];

export const pangasfalls = [
  //require('./../images/pangas2.jpg'),
  require('./../images/pangas.jpg'),
  require('./../images/pangas3.jpg'),
  require('./../images/pangas4.jpg'),
  require('./../images/pangas5.jpg'),
  require('./../images/pangas6.jpg'),
  require('./../images/pangas7.jpg'),
  //require('./../images/pangas8.jpg'),
];

export const tubigloon = [
  // require('./../images/tubigloon2.jpg'),
  // require('./../images/tubigloon3.jpg'),
  // require('./../images/tubigloon4.jpg'),
  require('./../images/tubigloon6.jpg'),
  require('./../images/tubigloon7.jpg'),
];

export const damagan = [
  //require('./../images/damagan3.jpg'), 
  //require('./../images/damagan4.jpg'),
  //require('./../images/damagan5.jpg'),
  //require('./../images/damagan16.jpg'),
  //require('./../images/damagan5.jpg'),
  require('./../images/damagan7.jpg'),
  require('./../images/damagan8.jpg'),
  require('./../images/damagan9.jpg'),
  require('./../images/damagan10.jpg'),
  require('./../images/damagan11.jpg'),
  require('./../images/damagan12.jpg'),
  //require('./../images/damagan13.jpg'),

];

export const kamira = [
  require('./../images/kamira2.jpg'),
  // require('./../images/kamira3.jpg'),
  // require('./../images/kamira4.jpg'),
];

export const chap = [
  require('./../images/chap2.jpg'),
  require('./../images/chap3.jpg'),
  require('./../images/chap4.jpg'),
];


export const danaoadventurepark = [
  require('./../images/dap3.jpg'),
  require('./../images/dap5.jpg'),
  require('./../images/dap4.jpg'),
  require('./../images/dap7.jpg'),
  require('./../images/dap8.jpg'),
  require('./../images/dap9.jpg'),
  require('./../images/dap10.jpg'),
  require('./../images/dap11.jpg'),
  require('./../images/dap12.jpg'),
  require('./../images/dap13.jpg'),
  require('./../images/dap14.jpg'),
  require('./../images/dap15.jpg'),
  require('./../images/dap16.jpg'),
];

export const haboog = [
  //require('./../images/haboog2.jpg'),
  //require('./../images/haboog3.jpg'),
  require('./../images/haboog4.jpg'),
  //require('./../images/haboog5.jpg'),
];

export const bigcross = [
  //require('./../images/bigcross2.jpg'),
  //require('./../images/bigcross3.jpg'),
  require('./../images/bigcross4.jpg'),
  require('./../images/bigcross5.jpg'),
  require('./../images/bigcross6.jpg'),
  require('./../images/bigcross7.jpg'),
  require('./../images/bigcross8.jpg')
];

export const morninghills = [
  require('./../images/hills2.jpg'),
  require('./../images/hills1.jpg'),
  require('./../images/morninghills2.jpg'),
  require('./../images/morninghills3.jpg'),
  require('./../images/morninghills4.jpg'),
  // require('./../images/morninghills5.jpg'),
  // require('./../images/morninghills6.jpg'),
  // require('./../images/morninghills7.jpg'),
  require('./../images/morninghills5.jpg'),
  require('./../images/morninghills6.jpg'),
  require('./../images/morninghills7.jpg')
];

  export const binolanon = [
    require('./../images/eskaya.jpg'),
  
  ];

 export const canubabeach = [
   //require('./../images/canuba2.jpg'),
   //require('./../images/canuba3.jpg'),
   //require('./../images/canuba4.jpg'),
   //require('./../images/canuba5.jpg'),
  // require('./../images/canuba6.jpg'),
  require('./../images/canuba6.jpg'),
  require('./../images/canuba7.jpg'),
  require('./../images/canuba8.jpg'),
  require('./../images/canuba9.jpg'),
  require('./../images/canuba10.jpg'),
  require('./../images/canuba11.jpg'),
  require('./../images/canuba12.jpg'),

 ];

 export const layabeach = [
   require('./../images/laya2.jpg'),
   require('./../images/laya3.jpg'),
   require('./../images/laya4.jpg'),
   require('./../images/laya5.jpg'),
   //require('./../images/laya6.jpg'),
 ];

 export const santafebeach = [
 require('./../images/santafe1.jpg'),
   require('./../images/santafe2.jpg'),
    require('./../images/santafe3.jpg'),
   require('./../images/santafe4.jpg'),
 ];

 export const daobeach = [
   require('./../images/dao2.jpg'),
   //require('./../images/dao3.jpg'),
   require('./../images/dao4.jpg'),
   require('./../images/dao5.jpg'),

 ];
 export const danaobeach = [
   require('./../images/danao2.jpg'),
   //require('./../images/danao3.jpg'),
   //require('./../images/danao4.jpg'),
   //require('./../images/danao5.jpg'),
   require('./../images/danao6.jpg'),
  require('./../images/danao7.jpg'),
  //require('./../images/danao8.jpg'),
  //require('./../images/danao9.jpg'),
 //require('./../images/danao10.jpg'),
  //require('./../images/danao11.jpg'),
 ];

 export const bagobobeach = [
   //require('./../images/bagobo2.jpg'),
   //require('./../images/bagobo3.jpg'),
   //require('./../images/bagobo4.jpg'),
   //require('./../images/bagobo5.jpg'),
   //require('./../images/bagobo6.jpg'),
   require('./../images/bagobo7.jpg'),
   require('./../images/bagobo8.jpg'),
   require('./../images/bagobo9.jpg'),
   require('./../images/bagobo10.jpg'),
   require('./../images/bagobo11.jpg'),

 ];

 export const sanisidrobeach = [
   require('./../images/sanisidro2.jpg'),
   require('./../images/sanisidro3.jpg'),
   //require('./../images/sanisidro4.jpg'),
   require('./../images/sanisidro5.jpg'),
 ];
 export const momobeach = [
   require('./../images/momo2.jpg'),
   require('./../images/momo3.jpg'),
   require('./../images/momo4.jpg'),
   require('./../images/momo5.jpg'),
   require('./../images/momo6.jpg'),
  require('./../images/momo7.jpg'),
  require('./../images/momo8.jpg'),
 ];

 export const bikinibeach = [
   //require('./../images/bikini2.jpg'),
   //require('./../images/bikini3.jpg'),
   require('./../images/bikini4.jpg'),
   //require('./../images/bikini5.jpg'),
  require('./../images/bikini6.jpg'),
  require('./../images/bikini7.jpg'),
  require('./../images/bikini8.jpg'),
  require('./../images/bikini9.jpg'),
 ];

  export const doljobeach = [
   //require('./../images/doljo2.jpg'),
   require('./../images/doljo.jpg'),
   require('./../images/doljo3.jpg'),
   //require('./../images/doljo5.jpg'),
    require('./../images/doljo6.jpg'),
    require('./../images/doljo7.jpg'),
    require('./../images/doljo8.jpg'),
 ];
  export const sevillahangingbridge = [
   //require('./../images/sevilla2.jpg'),
   //require('./../images/sevilla3.jpg'),
   //require('./../images/sevilla4.jpg'),
   //require('./../images/sevilla5.jpg'),
    require('./../images/sevilla6.jpg'),
    require('./../images/sevilla7.jpg'),
    require('./../images/sevilla8.jpg'),
    require('./../images/sevilla9.jpg'),
    require('./../images/sevilla10.jpg'),
 ];
  export const seaofclouds = [
require('./../images/clouds6.jpg'),
require('./../images/clouds7.jpg'),
require('./../images/clouds8.jpg'),
require('./../images/clouds9.jpg'),
require('./../images/clouds10.jpg'),
require('./../images/clouds11.jpg'),
require('./../images/clouds12.jpg'),

  //  require('./../images/clouds2.jpg'),
  //  require('./../images/clouds3.jpg'),
  //  require('./../images/clouds4.jpg'),
  //  require('./../images/clouds5.jpg'),
 ];

 export const panglaobeach = [
   require('./../images/bingag2.jpg'),
   require('./../images/bingag3.jpg'),
   //require('./../images/bingag4.jpg'),
   require('./../images/bingag5.jpg'),
 ];

 export const amarelabeach = [
   require('./../images/amarela2.jpg'),
   require('./../images/amarela3.jpg'),
   require('./../images/amarela4.jpg'),
   require('./../images/amarela5.jpg'),
   require('./../images/amarela6.jpg'),
   require('./../images/amarela7.jpg'),
   require('./../images/amarela8.jpg'),
 ];

export const phone = {
  globe1 : require('./../images/globe1.jpg'),
  globe2 : require('./../images/globe2.jpg'),
  globe3 : require('./../images/globe3.jpg'),
  globe4 : require('./../images/globe4.jpg'),
}

export const carlospgarcia = [
require('./../images/cpg4.jpg'),
require('./../images/cpg5.jpg'),
require('./../images/cpg6.jpg'),
];

export const boholnationalmuseum = [
require('./../images/museum1.jpg'),
require('./../images/museum2.jpg'),
require('./../images/museum3.jpg'),
require('./../images/museum4.jpg'),
require('./../images/museum5.jpg'),
require('./../images/museum6.jpg'),
require('./../images/museum7.jpg'),
require('./../images/museum8.jpg'),
];

export const provincialcapitolbuilding = [
require('./../images/capitol1.jpg'),
require('./../images/capitol2.jpg'),
];

export const cathedralsaintjoseph = [
require('./../images/cathedral4.jpg'),
require('./../images/cathedral5.jpg'),
require('./../images/cathedral6.jpg'),
];

export const sitioubos = [
require('./../images/sitio6.jpg'),
require('./../images/sitio7.jpg'),

];

export const mabawreef = [
require('./../images/mabaw.jpg'),

];

export const balicasagdive = [
  require('./../images/balicasagdive.jpg'),
  require('./../images/balicasagdive1.jpg'),
];
export const danajon = [
  require('./../images/danahon.jpg'),
  require('./../images/danahon1.jpg'),
];
export const cabilaodive = [
  require('./../images/cabilaodive.jpg'),
  require('./../images/cabilaodive1.jpg'),
];
export const pamilacandive = [
  require('./../images/pamilacandive.jpg'),
  require('./../images/pamilacandive1.jpg'),
];
export const shipwreck = [
  require('./../images/habagat.jpg'),
  require('./../images/habagat1.jpg'),
];
export const gardeneels = [
  require('./../images/gardeneels.jpg'),
];
export const snakeisland = [
  require('./../images/cervera.jpg'),
];

export const bienunidodive = [
  //require('./../images/bienunidive1.jpg'),
  require('./../images/bienunidive2.jpg'),
  // require('./../images/bienunidive3.jpg'),
  // require('./../images/bienunidive4.jpg'),
  // require('./../images/bienunidive5.jpg'),
  // require('./../images/bienunidive6.jpg'),
  //require('./../images/bienunidive7.jpg'),
];

export const fiestas = [
  require('./../images/fiesta.jpg'),
  //require('./../images/fiesta2.jpg'),
  //require('./../images/fiesta3.jpg'),
  //require('./../images/fiesta4.jpg'),
  //require('./../images/fiesta5.jpg'),

];

export const doljopoint = [
  require('./../images/doljo.jpg'),
  require('./../images/doljo1.jpg'),
];


export const tarsierconservationareabilar= [
  require('./../images/bilartarsier1.jpg'),
  require('./../images/bilartarsier2.jpg'),
  require('./../images/bilartarsier3.jpg'),

];

export const noimage = require('./../images/Ads3.jpg');
export const noimagebanner = require('./../images/noimage.jpg');
export const addbusiness = require('./../images/Ads2.jpg');
