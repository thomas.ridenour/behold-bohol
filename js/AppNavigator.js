'use strict';

import React, { Component } from 'react';
import { BackAndroid, Platform, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash/core';
import { Drawer } from 'native-base';
import { popRoute } from './actions/route';
import Navigator from 'Navigator';
import SideBar from './components/sideBar';
import { statusBarColor } from './themes/base-theme';

//views
import Main from './components/Main';
import DetailView from './components/DetailView';
import Nearby from './components/Nearby';
import Facts from './components/Facts';
import Vote from './components/Vote';
import Gallery from './components/Gallery';
import PDFView from './components/PDFView';
import Blogs from './components/Blogs';
import Loading from './components/Loading';
import Menu from './components/Menu';
import RadioStation from './components/Radio';
import modelen from './../assets/data/tarshare-en.json';
import modelzh from './../assets/data/tarshare-zh-CN.json';
import modelja from './../assets/data/tarshare-ja.json';
import modelko from './../assets/data/tarshare-ko.json';
import modeljstw from './../assets/data/tarshare-zh-TW.json';
import modelfr from './../assets/data/tarshare-fr.json';
import modelde from './../assets/data/tarshare-de.json';
import modelit from './../assets/data/tarshare-it.json';
import modeles from './../assets/data/tarshare-es.json';

import I18n from './../assets/data/translate.js';

Navigator.prototype.replaceWithAnimation = function (route) {
    const activeLength = this.state.presentedIndex + 1;
    const activeStack = this.state.routeStack.slice(0, activeLength);
    const activeAnimationConfigStack = this.state.sceneConfigStack.slice(0, activeLength);
    const nextStack = activeStack.concat([route]);
    const destIndex = nextStack.length - 1;
    const nextSceneConfig = this.props.configureScene(route, nextStack);
    const nextAnimationConfigStack = activeAnimationConfigStack.concat([nextSceneConfig]);

    const replacedStack = activeStack.slice(0, activeLength - 1).concat([route]);
    this._emitWillFocus(nextStack[destIndex]);
    this.setState({
        routeStack: nextStack,
        sceneConfigStack: nextAnimationConfigStack,
    }, () => {
        this._enableScene(destIndex);
        this._transitionTo(destIndex, nextSceneConfig.defaultTransitionVelocity, null, () => {
            this.immediatelyResetRouteStack(replacedStack);
        });
    });
};

export var globalNav = {};

const searchResultRegexp = /^search\/(.*)$/;

const reducerCreate = params=>{
    const defaultReducer = Reducer(params);
    return (state, action)=>{
        var currentState = state;

        if(currentState){
            while (currentState.children){
                currentState = currentState.children[currentState.index]
            }
        }
        return defaultReducer(state, action);
    }
};

const drawerStyle  = { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3};

class AppNavigator extends Component {
    constructor(props){
        super(props);

    }

    componentDidMount() {
        globalNav.navigator = this._navigator;

        BackAndroid.addEventListener('hardwareBackPress', () => {
            var routes = this._navigator.getCurrentRoutes();
            if(routes[routes.length - 1].id == 'main' || routes[routes.length - 1].id == 'loading') {
                //return false;
                BackAndroid.exitApp();
                return false;
            }
            else {
                this.popRoute();
                return true;
            }
        });
    }

    popRoute() {
        this.props.popRoute();
    }

    render() {
      var themodel = modelen;

      switch(I18n.locale){
        case 'es':
          themodel = modeles;
          break;
        case 'it':
          themodel = modelit;
          break;
        case 'fr':
          themodel = modelfr;
          break;
        case 'de':
            themodel = modelde;
            break;
        case 'ja':
        case 'ja-JP':
          themodel = modelja;
          break;
        case 'ko':
        case 'ko-KR':
          themodel = modelko;
          break;
        case 'zh':
        case 'zh-Hans':
        case 'zh-Hans-CN':
        case 'zh-Hans-HK':
        case 'zh-Hans-MO':
        case 'zh-Hans-SG':
          themodel = modelzh;
          break;
        case 'zh-Hant':
        case 'zh-Hant-HK':
        case 'zh-Hant-MO':
        case 'zh-Hant-TW':
          themodel = modelzh;
          break;
      }

        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                type='overlay'
                content={<SideBar navigator={this._navigator} />}
                tapToClose={true}
                acceptPan={false}
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                negotiatePan={true}>

                <Navigator
                    ref={(ref) => this._navigator = ref}
                    configureScene={(route) => {
                        return {
                            ...Navigator.SceneConfigs.FloatFromRight,
                            gestures: {}
                        };
                    }}
                    initialRoute={{id:'loading', statusBarHidden: true, model: themodel}}
                    renderScene={this.renderScene}
                  />
            </Drawer>
        );
    }

    renderScene(route, navigator) {
      console.disableYellowBox = true;
        if(route.component) {
            var Component = route.component;
            return (
                <Component navigator={navigator} route={route} {...route.passProps} />
            );
        }
        let model = route.model;
        switch (route.id) {
            case 'loading':
                return <Loading navigator={navigator} {...route.passProps}  model={model} />;
            case 'main':
                return <Main navigator={navigator} {...route.passProps} model={model} />;
            case 'detailView':
                return <DetailView navigator={navigator} {...route.passProps}/>;
            case 'nearby':
                return <Nearby navigator={navigator} {...route.passProps}/>;
            case 'facts':
                return <Facts navigator={navigator} {...route.passProps}/>;
            case 'vote':
                return <Vote navigator={navigator} {...route.passProps}/>;
            case 'gallery':
                return <Gallery navigator={navigator} {...route.passProps}/>;
            case 'pdfview':
                return <PDFView navigator={navigator} {...route.passProps}/>;
            case 'menu':
                return <Menu navigator={navigator} {...route.passProps}/>;
            case 'blogs':
                return <Blogs navigator={navigator} {...route.passProps}/>;
            case 'radioStation':
                return <RadioStation navigator={navigator} {...route.passProps}/>;
            default :
                return <Loading navigator={navigator} {...route.passProps}  model={model} />;
        }
    }
}

function bindAction(dispatch) {
    return {
        popRoute: () => dispatch(popRoute())
    }
}

const mapStateToProps = (state) => {
    return {
        drawerState: state.drawer.drawerState
    }
}

export default connect(mapStateToProps, bindAction) (AppNavigator);
