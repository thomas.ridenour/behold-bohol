'use strict';

import store from 'react-native-simple-store';
const CONFIGURE = require('./../constants/configure.js');

const api = {

  genericRequest(path, method, body ){
    let self = this;
    let options = {
      method: method || 'get',
      headers: {
        "Content-Type": 'application/json',
      }
    };

    if(body){
      options.body = JSON.stringify(body);
    }

   return fetch(CONFIGURE.SERVER + CONFIGURE.ENV + path, options).then((r) => self.checkResponse(r)).then((r) => {
        return r.json();
    });
  },

  checkResponse(response){
    if(typeof response === 'undefined' || response.status > 299 ){
      throw new Error('Error in communicating with server.');
    }
    return response;
  },

  serializeJSON(data) {
    return Object.keys(data).map(function (keyName) {
      return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
    }).join('&');
  },

  getEvents(lastPull=0){
   return this.genericRequest('events?product=' + CONFIGURE.PRODUCT + '&startdate=' + new Date().toISOString().split('T')[0] + '&lastPull='+ lastPull, 'get');
 },

  getBiz(lastPull=0){
    return this.genericRequest('biz?product=' + CONFIGURE.PRODUCT + '&lastPull='+ lastPull , 'get');
  },

  getPublic(lastPull=0){
    return this.genericRequest('public?product=' + CONFIGURE.PRODUCT + '&lastPull='+ lastPull , 'get');
  },

  vote(obj){
    return this.genericRequest('vote', 'post', {id: obj.id, product: CONFIGURE.PRODUCT, deviceid: obj.deviceid});
  },

  getVotes(){
    return this.genericRequest('vote2', 'get');
  },

  voteCampaign(obj){
    return this.genericRequest('voteall', 'post', {id: obj.id, product: CONFIGURE.PRODUCT, deviceid: obj.deviceid, campaign: obj.campaign});
  },

  getVotesCampaign(campaign){
    return this.genericRequest('voteall?campaign=' + campaign, 'get');
  },

  tracking(obj){
    obj.product = CONFIGURE.PRODUCT;
    obj.deviceid = obj.id;
    return this.genericRequest('tracking', 'post', obj);
  }
}

module.exports = api;
