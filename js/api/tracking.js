'use strict';

import store from 'react-native-simple-store';
const CONFIGURE = require('./../constants/configure.js');

const tracking = {
  vote2(obj){
    return this.genericRequest('vote2', 'post', {id: obj.id, product: CONFIGURE.PRODUCT, deviceid: obj.deviceid});
  },
}

module.exports = tracking;
