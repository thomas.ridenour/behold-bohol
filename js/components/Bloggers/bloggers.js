'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight,
  Image,
  Platform,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  Linking
} from 'react-native';


// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');
// var PRODUCT = 'bohol';
// var PATH = 'https://s3-ap-southeast-1.amazonaws.com/tarshare/';
import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';

const COLORS = require('./../../constants/colors');
var images = require('../../../assets/data/images');
import theme from '../../themes/base-theme';
//import { openDrawer } from '../../actions/drawer';
import SearchBar from 'react-native-searchbar';

import { Container, Content, List, ListItem, Header,InputGroup, Input, Icon, Button, Title, Thumbnail, Text } from 'native-base';


import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';

const CONFIGURE = require('./../../constants/configure');


class bloggers extends Component{

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      bloggers: undefined,
      model: this.props.model,
      searching: false,
    }
  }

  componentWillMount() {
    this.getData();
  }

  getData(){
    if(this.state.model && this.state.model.blogs){
      var k = lodash.filter(this.state.model['blogs'], (obj)=>{
        return(obj.category === 'bloggers')
      });
      this.setState({bloggers: k});
    }
  }

  search=()=>{
    this.state.searching = !this.state.searching ;
    this.state.searching ? this.searchBar.show() : this.searchBar.hide();
  }

  _handleResults=(data)=>{
    if(this.searchBar.getValue().length < 2){
      this.getData(this.state.model.blogs);
    } else {
      this.getData(data);
    }
  }

  _handleHide=(input)=>{
    this.getData(this.state.model.blogs);
  }

  gotoblog=(id)=>{
    this.props.pushNewRoute('blogs', {id, model: this.state.model});
  }

  gotoMap=()=>{
      this.props.pushNewRoute('nearby', { model: this.props.model });
  }

  render(){
    let source = CONFIGURE.IMAGE_PATH + 'bloggers' + '/';
    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>

        <Header>
          <View style={styles.header} >
    				<View style={styles.rowHeader}>
              <Button transparent onPress={this.search} style={{ width: 65, alignSelf: 'center'}}>
                <Icon name='ios-search-outline' color={'#fff'} style={{color: '#1C499E', fontSize: 30}} />
              </Button>

    					<Image source={require('../../../images/logo_small.png')} style={{flex: 1, height: 90, width: 90, resizeMode: 'contain', alignSelf: 'center'}}>
    					</Image>

              <Button transparent onPress={this.gotoMap} style={{ width: 65, alignSelf: 'center'}}>
                { (!CONFIGURE.HUAWEI) ?
                    <Icon name='ios-map-outline'  style={{color: '#1C499E', fontSize: 30} } /> :
                      <View/>
                }
              </Button>

    				</View>
    			</View>
        </Header>
          <View>

            <List dataArray={this.state.bloggers}
                renderRow={(item) =>
                    <ListItem onPress={() => this.gotoblog(item.id)}>
                      <Thumbnail  size={70} source={{uri: source + item.images[0]}}/>
                      <Text>{item.name}</Text>
                      <Text>{item.title}</Text>
                        <Text style={{color: '#818286'}} note>{item.description}</Text>
                        <Text style={{color: '#33691E', fontSize: 12, textAlign: 'right',  marginTop: 8}}> {I18n.t('gotoblog')} </Text>
                    </ListItem>
                }>
            </List>

            <SearchBar
              ref={(ref) => this.searchBar = ref}
              data={this.state.model.blogs}
              placeholder={I18n.t('search')}
              handleResults={this._handleResults}
              onHide={this._handleHide}
              iOSPadding={false}
              clearOnShow={false}
              clearOnHide={false}
              autoCorrect={false}
              hideBack
              allDataOnEmptySearch={false}
            />
        </View>

    </Container>
    );
   }
}

function bindAction(dispatch) {
    return {
        //openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(bloggers);
