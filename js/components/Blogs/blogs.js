'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ListView,
  Image,
  Platform,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  Linking
} from 'react-native';


// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');
// var PRODUCT = 'bohol';
// var PATH = 'https://s3-ap-southeast-1.amazonaws.com/tarshare/';
import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';

const COLORS = require('./../../constants/colors');
var images = require('../../../assets/data/images');
import theme from '../../themes/base-theme';
//import { openDrawer } from '../../actions/drawer';
import SearchBar from 'react-native-searchbar';
import HeaderContent from './../headerContent/';

import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Card, CardItem, Thumbnail } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';

const CONFIGURE = require('./../../constants/configure');


class blogs extends Component{

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      model: this.props.model,
      searching: false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      })
    }
  }

  componentWillMount() {
    this.getData();
  }

  getData(){
    if(this.state.model && this.state.model.blogs){
      var k = lodash.filter(this.state.model['blogs'], (obj)=>{
        return(obj.category === 'blogs' && obj.blogger === this.props.id)
      });

      this.shapeData(k.sort(function(a, b) {
        return (new Date(a.startdate) - new Date(b.startdate)) * -1;
      }));
    }
  }

  launch(url){
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('******* Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('******* An error occurred', err));
  }

  shapeData(data){
    if(data.length === 0){
      return;
    }

    let monthname = [
      I18n.t("january"),
      I18n.t("february"),
      I18n.t("march"),
      I18n.t("april"),
      I18n.t("may"),
      I18n.t("june"),
      I18n.t("july"),
      I18n.t("august"),
      I18n.t("september"),
      I18n.t("october"),
      I18n.t("november"),
      I18n.t("december")
    ];

    let weekday = [
      I18n.t("sunday"),
      I18n.t("monday"),
      I18n.t("tuesday"),
      I18n.t("wednesday"),
      I18n.t("thursday"),
      I18n.t("friday"),
      I18n.t("saturday"),
    ];

    let tempDataBlob = {};
    let now = new Date();
    let today = monthname[now.getMonth()] + ' ' + now.getDate() ;

    data && data.length > 0 && data.forEach((obj, i) => {

      let date = new Date(obj.startdate);
      let day = weekday[date.getDay()];
      let d =  date.getDate();
      let month =  monthname[date.getMonth()];
      let key = month;

      if(!tempDataBlob[key]){
        tempDataBlob[key] = [];
      }
      obj.index = i++;
      obj.date = month + ' ' + d;
      obj.day = day;
      obj.month = month;
    });

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(data)
    });
  }

  renderCard(event){
    let i = 0;
    event.images = [CONFIGURE.IMAGE_PATH + event.category + '/' + event.image];

    return(
      <View key={event.id}>
          <Card style={{ flex: 0, margin: 10 }} >
              <CardItem style={{borderColor: '#fff'}}>
                <View style={styles.sectionEvent}>
                    <View style={[styles.titleContainer]}>
                      <Text style={[styles.postTitle7]}>
                        {event && event.title}
                      </Text>
                    </View>
                  </View>
              </CardItem>

              <CardItem style={{borderColor: '#fff'}}>

                  <Image
                    style={[styles.slideImage, {height: width * (event.factor ? event.factor : 0.58), resizeMode: 'contain', flex: 1}]}
                    source={{ uri: CONFIGURE.IMAGE_PATH + event.menu + '/' + event.image}}
                  />

              </CardItem>
              <CardItem style={{borderColor: '#fff'}}>
                <View>
                  <Text style={[styles.postTitle8]}>
                    {event && event.date}
                  </Text>
                    <Text></Text>
                    <Text style={[styles.eventDescription]}>{event.description}</Text>
                    <Text></Text>
                </View>
              </CardItem>

                <TouchableOpacity
                  onPress={() => this.launch(event.url)}
                  >
                  <Text style={{color: '#33691E', fontSize: 20, textAlign: 'center', marginBottom: 12}}> {I18n.t('readmore')} </Text>
                </TouchableOpacity>
         </Card>

     </View>
    );
  }

  search=()=>{
    this.state.searching = !this.state.searching ;
    this.state.searching ? this.searchBar.show() : this.searchBar.hide();
  }

  _handleResults=(data)=>{
    //0 or X reset?
    if(this.searchBar.getValue().length < 2){
      this.shapeData(this.state.model.blogs);
    } else {
      this.shapeData(data);
    }
  }

  _handleHide=(input)=>{
    this.shapeData(this.state.model.blogs);
  }



  render(){
    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>

        <Header>
          <HeaderContent/>
        </Header>
          <ScrollView>

            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderCard.bind(this)}
              style={styles.listView}
            />

            <SearchBar
              ref={(ref) => this.searchBar = ref}
              data={this.state.model.blogs}
              placeholder={I18n.t('search')}
              handleResults={this._handleResults}
              onHide={this._handleHide}
              iOSPadding={false}
              clearOnShow={false}
              clearOnHide={false}
              autoCorrect={false}
              hideBack
              allDataOnEmptySearch={false}
            />
        </ScrollView>

    </Container>
    );
   }
}

function bindAction(dispatch) {
    return {
        //openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(blogs);
