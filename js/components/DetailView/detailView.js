import React, { Component } from 'react';
import {
  View,
  WebView,
  ScrollView,
  StyleSheet,
  Text,
  Platform,
  Image,
  Dimensions,
  TouchableOpacity,
  Linking,
  NetInfo,
  InteractionManager
} from 'react-native';

import styles from '../../styles/style.js';
import Swiper from 'react-native-swiper';
var images = require('../../../assets/data/images');
import Icon from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/MaterialIcons';
import { Content, Container, Header, Title, Button, Icon2, InputGroup, Input, Spinner } from 'native-base';
import HeaderContent from './../headerContent/';
import theme from '../../themes/base-theme';
import I18n from '../../../assets/data/translate.js';
const COLORS = require('./../../constants/colors');
import MapView from 'react-native-maps';
const CONFIGURE = require('./../../constants/configure');
import store  from 'react-native-simple-store';
import lodash from 'lodash';
import Toast from 'react-native-root-toast';
import api from '../../api/tarshare.js';

import { connect } from 'react-redux';
import { popRoute } from '../../actions/route';

// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

class DetailView extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      device: {},
      isLoading: false,
      latitude: null,
      longitude: null,
      geotags: [],
      model: {},
      images: [],
      promos: [],
      enddate: new Date().toISOString().split('T')[0],
      slat: 0,
      slong: 0,
      loaded: false,
      favorite: false,
      favorites: {}
    }
  }

  componentWillMount(){
    NetInfo.isConnected.addEventListener('change', Function.prototype);
    store.get('device').then((device) => {
			this.setState({device: device});
		});


    store.get('favorites').then((favorites) => {
      this.setState({favorites});
      if(favorites[this.state.model.id] !== undefined){
        this.setState({favorite: true})
      }
    });

    this.setState({model : this.props.data});

    let one = [this.props.data.geotag] || [];
    let two = this.props.data.geotag2 || [];
    var three = one.concat(two);

    this.state.geotags = three;
    this.setState({geotags : three});

  }

  componentDidMount(){
    InteractionManager.runAfterInteractions(() => {
      this.setState({loaded: true});
    });

    navigator.geolocation.getCurrentPosition(
       (position) => {

         this.setState({
           slat:  position.coords.latitude,
           slong: position.coords.longitude
         });
         this.props.tracker && this.props.tracker.trackScreenView(this.state.model.id);
        },
       (error) => {

        this.props.tracker && this.props.tracker.trackScreenView(this.state.model.id);
       },
       {enableHighAccuracy: true, timeout: 6000, maximumAge: 1000}
    );
  }

  postTrack=(action)=>{
    let k = this.state.device;
    k.geotag ={};
    k.geotag.latitude = this.state.slat;
    k.geotag.longitude = this.state.slong;
    k.menu = this.state.model.menu;
    k.category = this.state.model.category;
    k.tagid = this.state.model.id;
    k.action = action;


    NetInfo.isConnected.fetch().then(connected => {
      if(connected){
        api.tracking(k).then(value => {

        }).catch((error) => {
          console.warn('error error ' + error);
        });
      }
    });
  }

  launch(url){
    if(url.includes('@')){
        url = 'mailto:' + url;
    } else if(!url.includes('http') && !isNaN(+'+ 65 () 123'.replace(/\D/g,''))){
        url = 'tel:' + url.replace(/\D/g,'');
        if(url.length !== 15)
          return;
    }


    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('******* Can\'t handle url: ' + url);
      } else {
        this.postTrack('contacts');
        this.props.tracker && this.props.tracker.trackEvent(this.state.model.id, 'contacts');

        return Linking.openURL(url);
      }
    }).catch(err => console.error('******* An error occurred', err));
  }

  save=()=>{
    store.get('favorites').then((favorites) => {
      var visa = {}
      if(favorites){
          visa = favorites;
      }
      let si = lodash.size(visa);

      if(visa[this.state.model.id] !== undefined){
        delete visa[this.state.model.id];
        store.save('favorites', visa);
        Toast.show(I18n.t('removedfromfavorites'), {
          duration: Toast.durations.LONG,
          position: Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
        });
        this.setState({favorite: false})

        return;
      }

      if(si < 30){
        visa[this.state.model.id] = this.state.model;
        this.setState({favorite: true})
        store.save('favorites', visa);
        Toast.show(I18n.t('savedtofavorites'), {
          duration: Toast.durations.LONG,
          position: Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
        });
      } else {
        Toast.show(I18n.t('favoritesisfull'), {
          duration: Toast.durations.LONG,
          position: Toast.positions.TOP,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
        });
      }
    });
  }

  share=()=>{
  }

   getDirections =()=>{

     var dlat =  this.state.model.geotag.latitude;
     var dlong = this.state.model.geotag.longitude;
     var slat = this.state.slat;
     var slong = this.state.slong;
     var url = '';

     if(slat && slong){
        url = (Platform.OS === 'android') ? 'http://maps.google.com/maps?saddr=' + slat + ',' + slong +  '&daddr=' + dlat + ',' + dlong
                        : 'http://maps.apple.com/maps?saddr=' + slat + ',' + slong +  '&daddr=' + dlat + ',' + dlong;
      } else {
        url = (Platform.OS === 'android') ? 'http://maps.google.com/maps?daddr=' + dlat + ',' + dlong
                : 'http://maps.apple.com/maps?daddr=' + dlat + ',' + dlong;
      }
      this.postTrack('directions');
      this.props.tracker && this.props.tracker.trackEvent(this.state.model.id, 'directions');

     Linking.canOpenURL(url).then(supported => {
       if (!supported) {
         console.log('******* Can\'t handle url: ' + url);
       } else {
         return Linking.openURL(url);
       }
     }).catch(err => console.error('******* An error occurred', err));
   }

  render() {
    let index= 0;
    var img ={};
    const enddate = new Date().toISOString().split('T')[0];

    const back =(Platform.OS === 'android') ? 'md-arrow-back' : 'ios-arrow-back-outline';
    const promos = this.state.model && this.state.model.promos && this.state.model.promos.filter(dt => dt.enddate > enddate);

    if(this.state.model && this.state.model.images){
      img = this.state.model.images.map(obj => {
        return {uri:   CONFIGURE.IMAGE_PATH + this.state.model.category + '/' + obj}
      });
    } else {
      img = this.state.model.id && images[this.state.model.id];
    }

    return (
        <Container theme={theme} style={{backgroundColor: '#fff'}}>
          <Header>
            <View style={styles.header} >
      				<View style={styles.rowHeader}>
      					<Button transparent onPress={() => this.props.popRoute()} style={{width:65, alignSelf: 'center'}}>
      						<Icon name={back} style={{color: '#1C499E', fontSize: 30}}/>
      					</Button>

      					<Image source={require('../../../images/logo_small.png')} style={{ flex: 1, height: 90, width: 90,  resizeMode: 'contain', justifyContent: 'center'}}>
      					</Image>

                {this.state.model.logo  ?
                      <Image
                        style={[styles.logo, {marginRight: 15, paddingBottom: 5, alignSelf: 'center'}]}
                        source={{uri: CONFIGURE.IMAGE_PATH + this.state.model.category + '/'  + this.state.model.logo}}
                      />: <View style={{width: 50}}/>
                }
      				</View>
      			</View>
          </Header>
          <ScrollView style={{marginTop: 5, backgroundColor: '#fff'}}>
               <Swiper style={{backgroundColor: '#fff'}}
                 showsButtons={false}
                 height={width * 0.6148}
                 loop={true}
                 autoplay
                 resizeMode={'contain'}
                 showsPagination={true}
                 paginationStyle={{position: 'absolute', top: 2, left: 0, right: 0, height: 13}}
                 dot={<View style={{backgroundColor: 'rgba(255,255,255,.3)', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
                 activeDot={<View style={{backgroundColor: '#fff', width: 13, height: 13, borderRadius: 7, marginLeft: 7, marginRight: 7}} />}
                 nextButton={<Text style={styles.swiperText}>›</Text>}
                 prevButton={<Text style={styles.swiperText}>‹</Text>}
                >
                {img && img.length > 0 ? img.map(obj => (
                  <View style={styles.slide}>
                    <Image
                      style={[styles.slideImage, {height: width * (this.state.model.factor ? this.state.model.factor : 0.6148)}]}
                      source={images.noimage}
                    >
                      <Image
                        style={[styles.slideImage, {height: width * (this.state.model.factor ? this.state.model.factor : 0.6148)}]}
                        source={obj}
                      />
                    </Image>
                  </View>
                )) : <View style={styles.slide}>
                  <Image
                    style={[styles.slideImage, {height: width * (this.state.model.factor ? this.state.model.factor : 0.6148)}]}
                    source={images.noimage}
                  >
                  </Image>
                </View>
                }
              </Swiper>

              <View style={[styles.headlineContainer7]}>
                  <View>
                    <Text style={styles.postTitle}>
                      {this.state.model.title}
                    </Text>
                    <Text style={styles.postLocation}>
                      {this.state.model.location}
                    </Text>
                  </View>
              </View>

              <View style={{alignItems: 'center', justifyContent: 'center' , flex: 1}}>
                {this.state.model.price && this.state.model.symbol  &&
                  <View >
                    <Text style={{color: COLORS.LINKS, fontSize: 15,  justifyContent: 'center'}}>{I18n.t('pricepoint')}{this.state.model.symbol}{this.state.model.price}{I18n.t('beforediscounts')}</Text>
                  </View>
                }

                {this.state.model.rating &&
                  <View  style={{flexDirection: 'row'}}>
                    <Text style={{color: COLORS.LINKS, fontSize: 15,  justifyContent: 'center'}}>{I18n.t('ratings')} {this.state.model.rating} </Text>
                    <Icon3 style={{alignSelf: 'center', justifyContent: 'center'}}  name={"star"}  size={16} color={'gold'}/>
                  </View>
                }
              </View>

              {this.state.model.reservation && this.state.model.reservation.length > 0 &&
                <View style={styles.addressContainer}>
                  <Button  onPress={() => {this.launch(this.state.model.reservation) }} style={{ width: width-30, alignSelf: 'center', justifyContent: 'center', backgroundColor: '#33691E'}} >
                    <Text style={{color: '#fff', fontSize: 18, textAlign: 'center', justifyContent: 'center'}}> {I18n.t('booknow')} </Text>
                  </Button>
                </View>
              }


              <View style={styles.addressContainer}>

                <TouchableOpacity
                  style={{ alignItems: 'flex-start'}}
                  onPress={()=> this.save()}
                  >
                  <View >
                    <Text style={styles.emojiButtons}> { this.state.favorite ? '❤️' :  '💛' }  </Text>
                    <Text style={{fontSize: 10, alignSelf: 'center', justifyContent: 'center'}} > {I18n.t('save')}</Text>
                  </View>
                </TouchableOpacity>

                {this.state.model.reservation && this.state.model.reservation.length > 0 ?
                  <TouchableOpacity
                    style={{ alignItems: 'flex-start'}}
                    onPress={() => {this.launch(this.state.model.reservation) }}
                    >
                    <View>
                      <Text style={styles.emojiButtons}>✅</Text>
                      <Text style={{fontSize: 10, alignSelf: 'center', justifyContent: 'center'}} > {I18n.t('reservation')}</Text>
                    </View>
                  </TouchableOpacity> : null
                }

                { ((!CONFIGURE.HUAWEI) && this.state.model.geotag) ?
                  <TouchableOpacity
                    style={{ alignItems: 'flex-start'}}
                    onPress={()=> this.getDirections()}
                    >
                    <View>
                      <Text style={styles.emojiButtons}>🗺️</Text>
                      <Text style={{fontSize: 10, alignSelf: 'center', justifyContent: 'center'}} > {I18n.t('directions')}</Text>
                    </View>
                  </TouchableOpacity> : null
                }
              </View>

              { promos &&
                promos.length > 0  &&
                  <View>
                    <View style={styles.sectionContainer}>
                      <Text style={styles.sectionText}>
                        {I18n.t('promos')}
                      </Text>
                    </View>


                       <View>
                         <TouchableOpacity
                            style={styles.contacts}
                            activeOpacity={0.3}
                            onPress={() => this.launch(obj.uri)}>
                            <View >
                                <Image
                                  style={[{height: width * (promos[0].factor ? promos[0].factor : 0.6148)}]}
                                  source={{uri: CONFIGURE.IMAGE_PATH + 'promos' + '/' + promos[0].image}}
                                />
                            </View>
                          </TouchableOpacity>
                     </View>
                  </View>
                 }


                 {this.state.model.sections &&
                   this.state.model.sections.map( obj => (
                   <View>
                     <View style={styles.sectionContainer}>
                       <Text style={styles.sectionText}>
                         {obj.title}
                       </Text>
                     </View>
                     <View>
                       {obj.text.length > 0 && obj.text.map((k,i) =>(
                          <View style={[styles.contactsContainer, {backgroundColor: '#fff'}]}>
                            <Text style={[styles.textButton]}>{k}</Text>
                          </View>
                       ))}
                     </View>
                  </View>
                 ))}

                 { this.state.model.contacts &&
                 <View style={styles.sectionContainer}>
                   <Text style={styles.sectionText}>
                     {I18n.t('contact')}
                   </Text>
                 </View>}
                 {this.state.model.contacts && this.state.model.contacts.map((obj,i) =>(
                     <TouchableOpacity
                        style={styles.contacts}
                        activeOpacity={0.3}
                        onPress={() => this.launch(obj.link)}>
                        <View style={[styles.contactsContainer, { borderBottomColor: COLORS.LILLY_WHITE, borderBottomWidth: 1}]}>
                          <Icon3  name={obj.icon}  size={20} color={COLORS.LINK} style={{paddingRight: 6}}/>
                          <Text style={[styles.textButton]}>{obj.link}</Text>
                        </View>
                      </TouchableOpacity>
                 ))}

                 {this.state.model.facebook && this.state.model.facebook.length > 0 ?
                   <View
                     style={[styles.contactsContainer, { borderBottomColor: COLORS.LILLY_WHITE, borderBottomWidth: 1}]}
                     >
                     {this.state.model.facebook &&
                       <TouchableOpacity
                         style={{ flex:1, alignItems: 'center'}}
                         onPress={() => this.launch(this.state.model.facebook)}
                         >
                         <Icon  name={"logo-facebook"}  size={40} color={COLORS.FACEBOOK}/>
                       </TouchableOpacity>
                     }
                     {this.state.model.youtube && this.state.model.youtube.length > 0 &&
                       <TouchableOpacity
                         style={{flex:1, alignItems: 'center'}}
                         onPress={() => this.launch(this.state.model.youtube)}
                         >
                         <Icon  name={"logo-youtube"}  size={40} color={COLORS.YOUTUBE}/>
                       </TouchableOpacity>
                     }
                     {this.state.model.instagram &&
                       <TouchableOpacity
                         style={{ flex:1, alignItems: 'center'}}
                         onPress={() => this.launch(this.state.model.instagram)}
                         >
                         <Icon  name={"logo-instagram"}  size={40} color={COLORS.INSTAGRAM}/>
                       </TouchableOpacity>
                     }

                     {this.state.model.twitter &&
                       <TouchableOpacity
                         style={{ flex:1, alignItems: 'center'}}
                         onPress={() => this.launch(this.state.model.twitter)}
                         >
                         <Icon  name={"logo-twitter"}  size={40} color={COLORS.TWITTER}/>
                       </TouchableOpacity>
                     }

                     {this.state.model.googleplus &&
                       <TouchableOpacity
                         style={{ flex:1, alignItems: 'center'}}
                         onPress={() => this.launch(this.state.model.googleplus)}
                         >
                         <Icon  name={"logo-googleplus"}  size={40} color={COLORS.GOOGLEPLUS}/>
                       </TouchableOpacity>
                     }
                   </View>: null
                 }

                { this.state.model.hours &&
                  this.state.model.hours.length > 0 &&
                <View style={{marginTop: 12}}>
                  <View style={styles.sectionContainer}>
                    <Text style={styles.sectionText}>
                      {I18n.t('openhours')}
                    </Text>
                  </View>

                  {this.state.model.hours.map(obj => (
                    <View style={[styles.featureContainer]}>
                      <Text style={[styles.hoursTitle,{fontWeight: '500'}]}> {obj.title} </Text>
                      <Text style={styles.hoursTitle}> {obj.text} 👐</Text>
                    </View>
                  ))}
                </View>

              }

                 { this.state.model.features &&
                 <View style={styles.sectionContainer}>
                   <Text style={styles.sectionText}>
                     {I18n.t('features')}
                   </Text>
                 </View>}

                 { this.state.model.features && this.state.model.features.length > 0 ?
                   <View style={styles.featureContainer}>
                     <Text style={styles.hoursTitle}> { this.state.model.features.map( obj => ( obj + ', ' ) )} </Text>
                   </View> : null
                 }


              { (!CONFIGURE.HUAWEI) &&
                this.state.loaded &&
                this.state.model.geotag &&
              <View style={styles.staticmap}>
                <MapView
                    style={{height: 300, margin: 10}}

                    region={{
                      latitude: this.state.model.geotag.latitude,
                      longitude: this.state.model.geotag.longitude,
                      latitudeDelta: 0.2,
                      longitudeDelta: 0.25
                    }}

                    showsCompass={false}
                    showsMyLocationButton={true}
                    showsUserLocation={true}
                    showsIndoors={false}
                    toolbarEnabled={true}
                    showsPointsOfInterest={true}
                    showsBuildings={true}
                    showsTraffic={false}
                    rotateEnabled={false}
                    pitchEnabled={false}
                    loadingEnabled
                    showsScale
                    scrollEnabled={true}
                    moveOnMarkerPress={false}
                  >
                  {this.state.geotags.length ? this.state.geotags.map( obj => (
                      <MapView.Marker
                       coordinate={obj}
                       title={this.state.model.title}
                       pinColor={COLORS.TARSHARE_COLOR}
                     /> )) : <MapView.Marker
                      coordinate={this.state.model.geotag}
                      title={this.state.model.title}
                      pinColor={COLORS.TARSHARE_COLOR}
                    />
                  }
               </MapView>
             </View>
              }

           { this.state.model.credits && this.state.model.credits.length > 0 &&
             this.state.model.credits.map( obj => (
             <View>
               <View style={styles.sectionContainer}>
                 <Text style={styles.sectionText}>
                   {obj.title}
                 </Text>
               </View>
               <View>

                   <TouchableOpacity
                      style={styles.contacts}
                      activeOpacity={0.3}
                      onPress={() => obj.url && this.launch(obj.url)}
                    >
                      <View style={[styles.contactsContainer, {backgroundColor: '#fff'}]}>
                        <Text style={[styles.textButton]}>{obj.text}</Text>
                      </View>
                    </TouchableOpacity>

               </View>
            </View>
           ))
           }

           { this.state.model.photocredit && this.state.model.photocredit.length > 0 &&
             this.state.model.photocredit.map( obj => (
             <View>
               <View style={styles.sectionContainer}>
                 <Text style={styles.sectionText}>
                   {obj.title}
                 </Text>
               </View>
               <View>

                   <TouchableOpacity
                      style={styles.contacts}
                      activeOpacity={0.3}
                      onPress={() => obj.url && this.launch(obj.url)}
                    >
                      <View style={[styles.contactsContainer, {backgroundColor: '#fff'}]}>
                        <Text style={[styles.textButton]}>{obj.text}</Text>
                      </View>
                    </TouchableOpacity>

               </View>
            </View>
           ))
           }

         </ScrollView>

          </Container>
      );
    }
}

function bindAction(dispatch) {
    return {
        popRoute:()=>dispatch(popRoute())
    }
}

export default connect(null, bindAction)(DetailView);
