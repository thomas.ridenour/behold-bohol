'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ListView,
  Image,
  Platform,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  SegmentedControlIOS
} from 'react-native';


// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';

const COLORS = require('./../../constants/colors');
const CONFIGURE = require('./../../constants/configure');

var images = require('../../../assets/data/images');
import theme from '../../themes/base-theme';
import SearchBar from 'react-native-searchbar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Card, CardItem, Thumbnail } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';

class events extends Component{

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      model: this.props.model,
      searching: false,
      movies: [],
      newevents: [],
      sortList:[],
      tab: 0,
      movieSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      }),
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      })
    }
  }

  componentWillMount() {
    this.getData();
  }

  getData(){
    if(this.state.model && this.state.model.events){
      this.shapeData(this.state.model.events.sort(function(a, b) {
        return (new Date(b.startdate) - new Date(a.startdate)) * -1;
      }));
    }
  }

  shapeData(data){
    if(data.length === 0){
      return;
    }

    let monthname = [
      I18n.t("january"),
      I18n.t("february"),
      I18n.t("march"),
      I18n.t("april"),
      I18n.t("may"),
      I18n.t("june"),
      I18n.t("july"),
      I18n.t("august"),
      I18n.t("september"),
      I18n.t("october"),
      I18n.t("november"),
      I18n.t("december")
    ];

    let weekday = [
      I18n.t("sunday"),
      I18n.t("monday"),
      I18n.t("tuesday"),
      I18n.t("wednesday"),
      I18n.t("thursday"),
      I18n.t("friday"),
      I18n.t("saturday"),
    ];
    let tempDataBlob = {};
    let now = new Date();
    let today = monthname[now.getMonth()] + ' ' + now.getDate() ;

    data && data.length > 0 && data.forEach((obj, i) => {
      if(obj.enddate){
         var dateend = new Date(obj.enddate);
          var dayend = weekday[dateend.getDay()];
          var dend =  dateend.getDate();
          var monthend =  monthname[dateend.getMonth()];
          var keyend = month;
      }
      let date = new Date(obj.startdate);
      let day = weekday[date.getDay()];
      let d =  date.getDate();
      let month =  monthname[date.getMonth()];
      let key = month;

      if(!tempDataBlob[key]){
        tempDataBlob[key] = [];
      }

      obj.index = i++;
      obj.date = month + ' ' + d;
      obj.dateend = monthend + ' ' + dend;
      obj.day = day;
      obj.month = month;
    });
    this.state.model.events = data;

    var k = lodash.filter(this.state.model['events'], (obj)=>{
      return(obj.category === 'movies')
    });

    var m = lodash.filter(this.state.model['events'], (obj)=>{
      return(obj.category === 'events')
    });

    if(k && k.length > 0){
      this.setState({sortList: [I18n.t('eventstab'), I18n.t('movies')]});
    }

    this.setState({
      model: this.state.model,
      newevents: m,
      movies: k,
      movieSource: this.state.movieSource.cloneWithRows(k),
      dataSource: this.state.dataSource.cloneWithRows(m)
    });
  }

  renderCard(event){
    return(
          <Card style={{margin: 10 }} key={event.id} >
              <CardItem style={{borderColor: '#fff'}}>
                <View style={styles.sectionEvent}>
                          <View style={[styles.titleContainer, {flex:2}]}>
                            <Text style={[styles.postTitle7]}>
                              {event && event.title}
                            </Text>
                          </View>
                          <View style={[styles.rowRight]}>
                            <View>
                              <View style={styles.eventRight}>
                                <Text style={[styles.postTitle8]}>
                                  {event && event.date}
                                </Text>
                                {event && event.dateend && event.dateend !== event.date &&
                                <Text style={[styles.postTitle8]}>
                                  {event && event.dateend}
                                </Text>
                                }
                              </View>
                              <View style={styles.eventRight}>
                                <Text style={[styles.postTitle8]}>
                                  {event && event.time}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </View>
              </CardItem>

              <CardItem style={{borderColor: '#fff'}}>

                  <Image
                    style={[styles.slideImage, {height: width * (event.factor ? event.factor : 0.58), resizeMode: 'contain', flex: 1}]}
                    source={{ uri: CONFIGURE.IMAGE_PATH + 'events'+ '/' + event.image}}
                  />

              </CardItem>
              <View style={{padding: 8}}>
                  <Text style={styles.eventDescription}>{event.location}</Text>
                  <Text></Text>
                  <Text style={styles.eventDescription}>{event.description}</Text>
              </View>
         </Card>
    );
  }

  search=()=>{
    this.state.searching = !this.state.searching ;
    this.state.searching ? this.searchBar.show() : this.searchBar.hide();
  }

  _handleResults=(data)=>{

    if(this.searchBar.getValue().length < 2){
      this.shapeData(this.state.model.events);
    } else {
      this.shapeData(data);
    }
  }

  _handleHide=(input)=>{
    this.shapeData(this.state.model.events);
  }

  gotoMap=()=>{
      this.props.pushNewRoute('nearby', { model: this.props.model });
  }
  setOrder=(val)=>{
    if(+val === 0 || +val === 1){
      this.setState({
        tab: +val
      });
    }
  }

  setDataAndroid=(val)=>{
    this.setOrder(val.i);
  }

  _onChange = (event) => {
    this.setOrder(event.nativeEvent.selectedSegmentIndex);
  }

  render(){
    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>
        <Header>
          <View style={styles.header} >
    				<View style={styles.rowHeader}>
              <Button transparent onPress={this.search} style={{ width: 65, alignSelf: 'center'}}>
                <Icon name='ios-search-outline' color={'#fff'} style={{color: '#1C8644', fontSize: 30}} />
              </Button>

    					<Image source={require('../../../images/logo_small.png')} style={{flex: 1, height: 72, width: 72, resizeMode: 'contain', alignSelf: 'center'}}>
    					</Image>

              <Button transparent onPress={this.gotoMap} style={{ width: 65, alignSelf: 'center'}}>
                { (!CONFIGURE.HUAWEI) ?
                    <Icon name='ios-map-outline'  style={{color: '#1C499E', fontSize: 30} } /> :
                      <View/>
                }
              </Button>

    				</View>
    			</View>
        </Header>
        <View>
              {this.state.sortList && this.state.sortList.length >0 &&
                <View>
                  {(Platform.OS === 'ios') ?
                    <SegmentedControlIOS
                          style={styles.menusegmentedcontrol}
                          tintColor={COLORS.TARSHARE_COLOR}
                          values={this.state.sortList}
                          selectedIndex={0}
                          momentary={false}
                          disabled={false}
                          onChange={this._onChange}
                          onValueChange={ (value) => this.setOrder(value.toLowerCase()) }/>:
                    <ScrollableTabView
                          initialPage={0}
                          style={{height: 50}}
                          scrollWithoutAnimation={true}
                          locked={true}
                          tabBarUnderlineColor={COLORS.TARSHARE_COLOR}
                          tabBarUnderlineStyle={{backgroundColor: COLORS.TARSHARE_COLOR }}
                          tabBarBackgroundColor={'#fff'}
                          tabBarActiveTextColor={COLORS.TARSHARE_COLOR}
                          tabBarInactiveTextColor={COLORS.LABEL_GRAY}
                          onChangeTab={ (val) => this.setDataAndroid(val) }
                        >
                       {this.state.sortList && this.state.sortList.map(obj => (
                           <Text tabLabel={obj} />
                       ))
                       }
                    </ScrollableTabView>
                  }
              </View>
            }
            {(this.state.tab === 0) &&
                <ListView
                  dataSource={this.state.dataSource}
                  renderRow={this.renderCard.bind(this)}
                  style={styles.listView}
                />
            }
            {(this.state.tab === 1) &&
                <ListView
                  dataSource={this.state.movieSource}
                  renderRow={this.renderCard.bind(this)}
                  style={styles.listView}
                />
            }

            <SearchBar
              ref={(ref) => this.searchBar = ref}
              data={this.state.model.events}
              placeholder={I18n.t('search')}
              handleResults={this._handleResults}
              onHide={this._handleHide}
              iOSPadding={false}
              clearOnShow={false}
              clearOnHide={false}
              autoCorrect={false}
              hideBack
              allDataOnEmptySearch={false}
            />
        </View>
    </Container>
    );
   }
}

function bindAction(dispatch) {
    return {
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(events);
