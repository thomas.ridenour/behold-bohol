'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ListView,
  Image,
  Platform,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView
} from 'react-native';


// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');
import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';

const COLORS = require('./../../constants/colors');
var images = require('../../../assets/data/images');
import theme from '../../themes/base-theme';
import { openDrawer } from '../../actions/drawer';
import SearchBar from 'react-native-searchbar';

import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Card, CardItem, Thumbnail } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';
import HeaderContent from './../headerContent/';

const CONFIGURE = require('./../../constants/configure');


class facts extends Component{

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      model: this.props.model,
      vote: [],
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      })
    }
  }

  componentWillMount() {
    this.getData();

  }

  getData(){
    if(this.state.model && this.state.model.culture){
      var k = lodash.filter(this.props.model.culture, (obj)=>{
        return(obj.category === 'facts')
      });
      this.setState({facts: k});
    }
  }

  renderCards(){
    let i = 0;
    let cards = this.state.facts && this.state.facts.reverse();
    return(
      <View>
        { cards && cards.map(event => {
          var uri = CONFIGURE.IMAGE_PATH + event.category + '/' + event.images[0];
          return(
          <Card style={{ flex: 0, margin: 10 }} >
              <CardItem>
                <View style={styles.sectionEvent}>
                          <View style={[styles.titleContainer]}>
                            <Text style={[styles.sectionText, {color: 'black'}]}>
                              {event && event.title}
                            </Text>
                          </View>

                        </View>
              </CardItem>

              <CardItem>
                <Image style={[styles.slideImage, {height: width * 0.58, resizeMode: 'contain'}]} source={{ uri: uri}} />
              </CardItem>

              <CardItem>
                <View>
                  <Text >
                      {event.sections && event.sections[0].text}
                  </Text>

                  { event.credits && event.credits.length > 0 &&
                        event.credits.map( obj => (
                              <TouchableOpacity
                                  style={styles.contacts}
                                >
                                <Text/>
                                <Text>{obj.title} {obj.text}</Text>
                              </TouchableOpacity>
                        ))
                  }

                   { event.photocredit && event.photocredit.length > 0 &&
                    event.photocredit.map( obj => (
                              <TouchableOpacity
                                  style={styles.contacts}
                                >
                                <Text/>
                                <Text>{obj.title} {obj.text}</Text>
                              </TouchableOpacity>
                      ))
                    }
                </View>
              </CardItem>




         </Card>

       )
       })}
     </View>
    );
  }

  gotoDetails=(data)=>{
    data.sections = [{
      text: [data.day + ' ' + data.date + ' ' + data.time],
      title: 'WHEN'
    },
      {
        text: [data.description],
        title: 'DESCRIPTION'
      }
    ];
    this.props.pushNewRoute('detailView', {  data: data });
  }

  search=()=>{
    this.searchBar.show();
  }

  _handleResults=(data)=>{
    if(this.searchBar.getValue().length < 2){
      this.shapeData(this.state.model.events);

    } else {
      this.shapeData(data);
    }
  }

  _handleHide=(input)=>{
    this.shapeData(this.state.model.events);
  }

  render(){
    return (
      <Container theme={theme}>
        <Header>
            <HeaderContent/>
        </Header>
          <ScrollView >

            {this.renderCards()}
            <SearchBar
              ref={(ref) => this.searchBar = ref}
              data={this.state.model.events}
              placeholder={I18n.t('search')}
              handleResults={this._handleResults}
              onHide={this._handleHide}
              iOSPadding={false}
              allDataOnEmptySearch
              clearOnShow={false}
              clearOnHide={false}
              autoCorrect={false}
            />
          </ScrollView>

    </Container>
    );
   }
}

function bindAction(dispatch) {
    return {
        openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(facts);
