'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ListView,
  Image,
  Platform,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView
} from 'react-native';


// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');
// var PRODUCT = 'bohol';
// var PATH = 'https://s3-ap-southeast-1.amazonaws.com/tarshare/';
import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';

const COLORS = require('./../../constants/colors');
var images = require('../../../assets/data/images');
import theme from '../../themes/base-theme';
//import { openDrawer } from '../../actions/drawer';
import SearchBar from 'react-native-searchbar';

import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Card, CardItem, Thumbnail } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';
import HeaderContent from './../headerContent/';

const CONFIGURE = require('./../../constants/configure');

var images = require('../../../assets/data/images');

class gallery extends Component{

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      model: this.props.data,
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      })
    }
  }

  componentWillMount() {
    this.getData();
  }

  getData(){
    //category towns
    this.setState({
        dataSource: this.state.dataSource.cloneWithRows(this.state.model.images)
    });
  }

  renderRow(obj){
    return (
        <Image
          style={{height: width * obj.factor , width: width - 24 , resizeMode: 'contain', flex: 1, alignSelf: 'center', marginTop: (obj.factor == 1 ) ? 12: 0 }}
          source={{ uri: CONFIGURE.IMAGE_PATH + this.state.model.category + '/' + obj.image}}
        />
    )
  }

  render(){
    return (
      <Container theme={theme}>
        <Header>
            <HeaderContent/>
        </Header>
          <View style={{backgroundColor: '#fff', paddingBottom: 12, flex: 1 }}>
            <Text style={{textAlign: 'center', paddingVertical: 12, backgroundColor: '#AB1264', color: '#fff', fontSize: 18, fontWeight: '600'}}>{this.state.model.title}</Text>

            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderRow.bind(this)}
            />
          </View>
    </Container>
    );
   }
}

function bindAction(dispatch) {
    return {
        //openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(gallery);
