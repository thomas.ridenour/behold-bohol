'use strict';

import React, { Component } from 'react';

import { Image, View, Dimensions, NetInfo, Platform,  Animated, Easing } from 'react-native';

import styles from '../../styles/style.js';

const COLORS = require('./../../constants/colors');
import lodash from 'lodash';

import api from '../../api/tarshare.js';
import store  from 'react-native-simple-store';
import { Spinner } from 'native-base';

// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

export default class loading extends Component {

  constructor (props) {
    super(props);
        this.state = {
          model: this.props.model
        }
      }

    componentWillMount () {
        NetInfo.isConnected.addEventListener('change', Function.prototype);

        store.get('master').then((master) => {
          if(master){
            this.unpack(master);
            this.fetchData(master.lastPull || 0);
          } else{
            this.fetchData(0);
          }
        })
    }

    unpack=(master)=>{

      let sections = ["accomodation","events", "dining", "touring", "shopping", "culture", "banking", "public2", "ads", "blogs", "saulog", "vote"];
      let enddate = new Date().toISOString().split('T')[0];

      lodash.remove(master["events"], function(n) {
        return n.enddate < enddate ;
      });

      lodash.remove(master["ads"], function(n) {
        return n.enddate < enddate ;
      });

      sections.forEach((obj) => {
           let k = lodash.unionBy( master[obj], this.state.model[obj], 'id');
           this.state.model[obj] = k;
      });
    }

    fetchData=(lastPull)=>{
      NetInfo.isConnected.fetch().then(connected => {
        if(connected){
            this.fetchtimeout(10000, Promise.all([api.getEvents(lastPull), api.getBiz(lastPull), api.getPublic(lastPull)])).then((responses) => {
              let model = {};

              responses.forEach( obj => {
                  obj.Items.forEach(item =>{
                    if(!model[item.menu]){
                      model[item.menu] = [];
                    }
                    item.menu && model[item.menu].push(item);
                  });
              });

              this.unpack(model);
              this.state.model.previousLastPull = this.state.model.lastPull;
              this.state.model.lastPull = Date.now();

            store.save('master', this.state.model).then((error) => {
              if(!error){
                //set the lastPull?
              }
              this.goNext(true);
            });

            }).catch((error) => {
              this.goNext();
            });

        } else{
          this.goNext();
        }
      });
    }

    fetchtimeout=(ms, promise)=>{
      return new Promise(function(resolve, reject) {
        setTimeout(function() {
          reject(new Error("timeout"))
        }, ms)
        promise.then(resolve, reject)
      })
    }

    goNext=(network=false)=>{
      this.state.model.network = network;
      this.props.navigator.replace({
        id:'main',
        model: this.state.model,
        network: network
      });
    }

    render () {
        return (
          <View style={styles.container}>
            <Image source={require('../../../images/logo.jpg')}
              style={{flex: 1, height: width, width: width, resizeMode: 'contain', alignItems: 'center', justifyContent: 'center'}}>
              <Spinner style={{paddingTop: 250}} color={'#1B3E95'}/>
            </Image>
          </View>
        );
    }
}
