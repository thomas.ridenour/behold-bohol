'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  View,
  TouchableHighlight,
  Text,
  Navigator,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  Platform,
  TabBarIOS

} from 'react-native';

import styles from '../../styles/style.js';
import Events from '../Events';
import Start from '../Start';
import Nearby from '../Nearby';
import Bloggers from '../Bloggers';
import I18n from '../../../assets/data/translate.js';
import Tartab from './tabbar';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar, } from 'react-native-scrollable-tab-view';
const COLORS = require('./../../constants/colors');

import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
let tracker = new GoogleAnalyticsTracker('UA-106746266-1');

export default class Main extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollableTabView
          initialPage={0}
          tabBarPosition={"bottom"}
          scrollWithoutAnimation={true}
          locked={true}
          renderTabBar={() => <Tartab labels={[I18n.t('home'), I18n.t('events'), I18n.t('blogs') ]}/>}>

            <ScrollView tabLabel="ios-home-outline" label="Home">
                <Start navigator={this.props.navigator}  model={this.props.model} network={this.props.network} tracker={tracker} />
            </ScrollView>

            <ScrollView  tabLabel="ios-calendar-outline" label="Events">
                <Events navigator={this.props.navigator}  model={this.props.model} tracker={tracker} />
            </ScrollView>

            <ScrollView  tabLabel="ios-create-outline" label="Blogs" style={styles.tabView}>
                <Bloggers navigator={this.props.navigator}  model={this.props.model} tracker={tracker} />
            </ScrollView>
          </ScrollableTabView>
      </View>
    );
  }

}
