
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Platform
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
const COLORS = require('./../../constants/colors');

const FacebookTabBar = React.createClass({
  tabIcons: [],

  propTypes: {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array,
  },

  componentDidMount() {
    this.setAnimationValue({ value: this.props.activeTab, });
    this._listener = this.props.scrollValue.addListener(this.setAnimationValue);
  },

  setAnimationValue({ value, }) {
    this.tabIcons.forEach((icon, i) => {
      const progress = (value - i >= 0 && value - i <= 1) ? value - i : 1;
    });
  },

  iconColor(progress) {
    const red = 28 + (151 - 23) * progress;
    const green = 73 + (154 - 156) * progress;
    const blue = 158 + (162 - 153) * progress;
    return `rgb(${red}, ${green}, ${blue})`;
  },

  render() {
    const tabWidth = this.props.containerWidth / this.props.tabs.length;
    const left = this.props.scrollValue.interpolate({
      inputRange: [0, 1, ], outputRange: [0, tabWidth, ],
    });

    return <View>
        <View style={[styles.tabs, this.props.style, ]}>
          {this.props.tabs.map((tab, i) => {
            return <TouchableOpacity key={tab} onPress={() => this.props.goToPage(i)} style={styles.tab}>
              <View style={styles.iconView}>
                <Icon
                  name={tab}
                  size={28}
                  color={this.props.activeTab == i ? '#1C499E' : '#979AA2'}
                  ref={(icon) => { this.tabIcons[i] = icon; }}
                >
                </Icon>

                  <Text
                    style={[styles.label, {color: this.props.activeTab == i ? '#1C499E' : '#979AA2' }]}
                    >
                    {this.props.labels[i]}
                  </Text>

              </View>
            </TouchableOpacity>;
          })}
        </View>
        <Animated.View style={[styles.tabUnderlineStyle, { width: tabWidth }, { left, }, ]} />
      </View>
  },
});

const styles = StyleSheet.create({
  iconView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label:{
    fontSize: 10,
    fontWeight: '700'
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    //paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  tabs: {
    height: Platform.OS === 'ios' ? 52 : 55,
    borderTopColor: COLORS.ACTIVE_ICON , //Platform.OS === 'ios' ? COLORS.ACTIVE_ICON : 'transparent',
    flexDirection: 'row',
    paddingTop: 3,
    paddingBottom: 2,
    borderWidth: 2,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 1,
    borderBottomColor: 'transparent',
  },
  tabUnderlineStyle: {
    position: 'absolute',
    height: 0,//(Platform.OS === 'ios')? 0: 3,
    backgroundColor: COLORS.ACTIVE_ICON,
    bottom: 0,
  },
});

export default FacebookTabBar;
