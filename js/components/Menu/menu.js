'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  ListView,
  Text,
  View,
  Image,
  Platform,
  Linking,
  SegmentedControlIOS,
  NetInfo,
  Dimensions,
  TouchableOpacity,
  InteractionManager
} from 'react-native';

import styles from '../../styles/style.js';
var I18n = require('react-native-i18n');
import { Icon,  Button, InputGroup, Input, Container, Header, Content, Spinner} from 'native-base';
import store  from 'react-native-simple-store';
import ScrollableTabView from 'react-native-scrollable-tab-view';
const COLORS = require('./../../constants/colors');
import Toast from 'react-native-root-toast';
import api from '../../api/tarshare.js';
import theme from '../../themes/base-theme';
var images = require('../../../assets/data/images');
import HeaderContent from './../headerContent/';
import lodash from 'lodash';
import { connect } from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';
const CONFIGURE = require('./../../constants/configure');
import { popRoute } from '../../actions/route';
import Icon3 from 'react-native-vector-icons/MaterialIcons';

// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

class menu extends Component{
  constructor(props){
    super(props);
    this.state = {
      ads: undefined,
      device: {},
      category: '',
      menu: '',
      showAddBusiness: false,
      isLoading: false,
      near: [],
      rating: [],
      price: [],
      promos: [],
      sortList: [I18n.t("near"), I18n.t("ratings")],
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
      tab: 0
    }
  }

  componentWillMount(){
    NetInfo.isConnected.addEventListener('change', Function.prototype);
    store.get('device').then((device) => {
      this.state.device = device;
      this.postTrack('view');
    });

    this.setState({
      category: this.props.category,
      menu: this.props.menu
    });
  }

  componentDidMount(){

    InteractionManager.runAfterInteractions(() => {
      this.fetchData();
    });
    this.props.tracker && this.props.tracker.trackScreenView(this.props.category);

    this.props.tracker && this.props.tracker.trackScreenView('Menu ', this.props.category);
  }

  postTrack=(action)=>{
    // if(!this.state.ads){
    //   return;
    // }

    this.state.ads && this.props.tracker && this.props.tracker.trackEvent(this.state.ads.id, 'ads');

    let k = this.state.device;
    k.geotag ={};
    k.menu = this.props.menu;
    k.category = this.props.category;
    k.tagid = this.state.ads.id;
    k.action = action;


    NetInfo.isConnected.fetch().then(connected => {
      if(connected){
        api.tracking(k).then(value => {
        }).catch((error) => {
          console.warn('AAA error ' + error);
        });
      }
    });
  }

  fetchData(){
    var near = lodash.filter(this.props.model[this.props.menu], (obj)=>{
      if(obj.menu === 'accomodation' || obj.menu === 'dining' || obj.menu === 'touring' || obj.menu === 'shopping')
        return(obj.category === this.props.category && obj.paid === true);
      return(obj.category === this.props.category);
    });

    if(this.props.category !== 'towns' && this.props.menu !== 'culture' && this.props.menu !== 'vote'){
      this.sortByDistance(near);
    }

    var ads = lodash.find(this.props.model.ads, (obj)=>{
      return(obj.adcategories.includes(this.props.category));
    });

    var rg = near.slice();
    var rating = rg.sort(function(a, b) {
        return (b.rating - a.rating);
      });

    var promos = [];
    let enddate = new Date().toISOString().split('T')[0];
    rating.forEach((obj, i) => {
      if(obj.promos && obj.promos.length > 0 && obj.promos[0].image && obj.enddate < enddate ){
        promos = promos.concat(obj.promos);
      }
    });

    if(promos.length > 0){
      this.setState({sortList: [I18n.t("near"), I18n.t("ratings"), I18n.t("promos")]});
    }

    this.setState({
      ads,
      rating,
      promos,
      loaded: true,
      dataSource: this.state.dataSource.cloneWithRows(near)
    });
  }

  sortByDistance=(k)=>{
    navigator.geolocation.getCurrentPosition(function () {}, function () {}, {});
    navigator.geolocation.getCurrentPosition(
       (position) => {
          //calculate distance
          k.forEach(obj=> {
            if(obj.geotag && obj.geotag.latitude && obj.geotag.longitude){
              obj.distance = this.distance(position.coords.latitude,
                    position.coords.longitude,
                    obj.geotag.latitude,
                    obj.geotag.longitude);
            } else{
              obj.distance = 9999;
            }

          });

          //sort
          var lk = k.slice();
          var jk = lk.sort(function(a, b) {
              return (a.distance - b.distance);
            });


          jk && jk.length > 0 && this.setState({
              loaded: true,
              near: jk,
              dataSource: this.state.dataSource.cloneWithRows(jk),
          });
       },
       (error) => {
          this.setState({
            loaded: true,
            k,
            dataSource: this.state.dataSource.cloneWithRows(k)
          })
       },
       {enableHighAccuracy: false, timeout: 5000, maximumAge: 5000}
    );
  }


  gotoDetails(data) {
    if(data.pdf && data.pdf.length > 0){
      this.props.pushNewRoute('pdfview', {data: data, tracker: this.props.tracker});
    } else {
      this.props.pushNewRoute('detailView', {  data: data, tracker: this.props.tracker });
    }
  }

  distance=(lat1, lon1, lat2, lon2)=> {
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
            c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
//(Math.round(row.distance * 100) / 100)
    return (Math.round(12742 * Math.asin(Math.sqrt(a)) * 100) / 100); // 2 * R; R = 6371 km
  }

  launch=(url)=>{
    if(url.includes('@')){
        url = 'mailto:' + url;
    } else if(!url.includes('http') && !isNaN(+'+ 65 () 123'.replace(/\D/g,''))){
        url = 'tel:' + url.replace(/\D/g,'');
    }

    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('******* Can\'t handle url: ' + url);
      } else {
        this.postTrack('click');
        this.props.tracker &&  this.props.tracker.trackEvent(this.state.ads.id, 'click');

        return Linking.openURL(url);
      }
    }).catch(err => console.error('******* An error occurred', err));
  }

  gotoMap=()=>{
      this.props.pushNewRoute('nearby', {  menu: this.props.menu, category: this.props.category, model: this.props.model });
  }

  renderRow(row) {
    let source = row.banner ? {uri: CONFIGURE.IMAGE_PATH + row.category + '/' +row.banner}  : images[this.props.menu] && images[this.props.menu][row.id] || undefined ;
    const info =   <View style={[styles.headlineContainer4, {flexDirection: 'row'}]}>
                      <View style={{flex:1}}>
                          <Text style={[styles.postTitle7]}>{row.title} </Text>
                            {row.address && (row.category !== 'ecotourpackage') && <Text style={styles.postTitle8}>{row.address}</Text>}
                            { (row.price || row.rating || row.distance) &&
                              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginRight: 24}}>
                                {row.price && <Text style={styles.postTitle8}>{row.symbol} {row.price} </Text>}
                                {row.rating && <Text style={[styles.postTitle8]}>  {row.rating} ⭐ </Text>}
                                {row.distance && row.distance !== 9999 && <Text style={styles.postTitle8}>{(row.distance + 'km')} </Text>}
                              </View>
                            }
                      </View>
                      {row.logo  &&
                        <View style={{width: 70, justifyContent: 'center'}}>
                            <Image
                              style={[styles.logo2]}
                              source={{uri: CONFIGURE.IMAGE_PATH + row.category + '/'  + row.logo}}
                            />
                        </View>
                      }
                  </View>;


    if(source){
        return (
            <TouchableHighlight
              key={row.id}
              style={styles.menuContainerList}
              onPress={() => this.gotoDetails(row)}
              underlayColor='transparent'>
                <View style={{backgroundColor: '#fff'}}>
                    <Image
                      source={ images.noimagebanner }
                      style={[styles.wholeimage2]}>
                      <View style={{flexDirection: 'row'}}>
                        <Image
                          source={source || images.noimagebanner}
                          style={[styles.wholeimage2]} >
                        </Image>
                      </View>
                    </Image>
                    {info}
                </View>
            </TouchableHighlight>
        );
      } else {
        return(
          <View key={row.id} style={[styles.menuContainerList]}>
            {info}
        </View>
        );
      }
  }

  setOrder=(val)=>{
    var order=[];
    if(+val === 0){
      order = this.state.near.slice();
    } else if(+val === 1){
      order = this.state.rating.slice();
    } else if(+val === 2){
      this.setState({
          tab: +val
      });
      return;
    }

    order.length > 0 && this.setState({
        dataSource: this.state.dataSource.cloneWithRows(order),
        tab: +val
    });
  }

  setDataAndroid=(val)=>{
    this.setOrder(val.i);
  }

  _onChange = (event) => {
    this.setOrder(event.nativeEvent.selectedSegmentIndex);
  }

  render(){
    let source = this.state.ads &&  {uri: CONFIGURE.IMAGE_PATH + this.props.category + '/' + this.state.ads.images[0]};
    let adsource = this.state.ads &&  {uri: CONFIGURE.IMAGE_PATH + 'ads' + '/' + this.state.ads.images[0]};
    let back =(Platform.OS === 'android') ? 'md-arrow-back' : 'ios-arrow-back-outline';

    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>
        <Header>
          <View style={styles.header} >
            <View style={styles.rowHeader}>
              <Button transparent onPress={() => this.props.popRoute()} style={{width:65, alignSelf: 'center'}}>
                <Icon name={back} style={{color: '#1C499E', fontSize: 30}}/>
              </Button>

              <Image source={require('../../../images/logo_small.png')} style={{flex: 1, height: 90, width: 90, resizeMode: 'contain', alignSelf: 'center'}}>
              </Image>

              <Button transparent style={[styles.btnHeader, {width:65, alignSelf: 'center', flexDirection: 'column', color: '#fff'}]} onPress={()=> this.gotoMap()}>
                { (!CONFIGURE.HUAWEI) ?
                    <Icon name='ios-map-outline'  style={{color: '#1C499E', fontSize: 30} } /> :
                      <View/>
                }
              </Button>
            </View>
          </View>
        </Header>


          {this.props.menu !== 'culture' &&
            this.props.menu !== 'vote' &&
            this.props.menu !== 'vote2' &&
            this.props.menu !== 'saulog' &&

            <View>
            {(Platform.OS === 'ios') ?
              <SegmentedControlIOS
                    style={styles.menusegmentedcontrol}
                    tintColor={COLORS.TARSHARE_COLOR}
                    values={this.state.sortList}
                    selectedIndex={0}
                    momentary={false}
                    disabled={false}
                    onChange={this._onChange}
                    onValueChange={ (value) => this.setOrder(value.toLowerCase()) }/>:
              <ScrollableTabView
                    initialPage={0}
                    style={{height: 50, marginBottom: 20}}
                    scrollWithoutAnimation={true}
                    locked={true}
                    tabBarUnderlineColor={COLORS.TARSHARE_COLOR}
                    tabBarUnderlineStyle={{backgroundColor: COLORS.TARSHARE_COLOR }}
                    tabBarBackgroundColor={'#fff'}
                    tabBarActiveTextColor={COLORS.TARSHARE_COLOR}
                    tabBarInactiveTextColor={COLORS.LABEL_GRAY}
                    onChangeTab={ (val) => this.setDataAndroid(val) }
                  >
                 {this.state.sortList && this.state.sortList.map(obj => (
                     <Text tabLabel={obj} />
                 ))
                 }
              </ScrollableTabView>
            }
            </View>
          }
          <Content>

          {this.state.ads &&
            source &&
            <TouchableHighlight
              style={{margin: 1}}
              underlayColor='trasparent'
              onPress={()=> {
                this.props.tracker && this.props.tracker.trackEvent(this.state.ads.id, 'adspressed');
                this.launch(this.state.ads.uri || '')}}
              >
              <View style={styles.listcontainer}>
                <Image
                  style={[styles.adimage, {width: width-2, height: width * (this.state.ads.factor ? this.state.ads.factor : 0.6848)}]}
                  source={images.noimage}
                >
                  <Image
                    source={adsource}
                    style={[styles.adimage, {width: width-2, height: width * (this.state.ads.factor ? this.state.ads.factor : 0.6848)}]} >
                  </Image>
                </Image>
              </View>
            </TouchableHighlight>
          }
          {(this.state.loaded === false) &&
            <Spinner color="#B1B2A6" size="large"/>
          }
          {this.state.tab !== 2 &&
            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderRow.bind(this)}
              style={[styles.listView, {paddingTop: 2}]}
            />
          }
          {this.state.tab === 2 &&
            this.state.promos.map(obj => (
              <TouchableOpacity
                 style={{margin:1}}
                 activeOpacity={0.3}
                 onPress={()=> {
                   this.props.tracker && this.props.tracker.trackEvent(obj.uri, 'promopressed');
                   this.launch(obj.uri || '')}}
                 >
                 <View >
                     <Image
                       style={[{height: width * (obj.factor ? obj.factor : 0.6148)}]}
                       source={{uri: CONFIGURE.IMAGE_PATH + 'promos' + '/' + obj.image}}
                     />
                 </View>
               </TouchableOpacity>

            ))
          }
      </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
    return {
        //openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops)),
        popRoute:()=>dispatch(popRoute())

    }
}

export default connect(null, bindAction)(menu);
