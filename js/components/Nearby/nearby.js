'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  NetInfo,
  TouchableHighlight,
  ListView,
  Image,
  Platform,
  TouchableWithoutFeedback,
  SegmentedControlIOS
} from 'react-native';

import MapView from 'react-native-maps';
import styles from '../../styles/style.js';
import I18n from '../../../assets/data/translate.js';
const PINCOLORS = require('../../../assets/data/lists.json').colors;
const COLORS = require('./../../constants/colors');
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import { Container, Header, InputGroup, Input, Icon, Button, Content, Title } from 'native-base';
import theme from '../../themes/base-theme';
import { pushNewRoute, replaceRoute } from '../../actions/route';
import { connect} from 'react-redux';
import lodash from 'lodash';

import HeaderContent from './../headerContent/';

class nearby extends React.Component{
  constructor(props){
    super(props);

    this.state ={
        geolocationData:[],
        allData:[],
        open: false,
        category:'',
        geotags: [],
        me:{
          latitude: 9.849850,
          longitude: 124.193542
        },
        value: 0,
        selectedOption:'',
        screen: 0,
      }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData(){
    var gt = [];
    var category = this.props.category;
    var md = this.props.model;
    var menu = this.props.menu;

    if(!category){
      lodash.forEach(md, (cat)=> {
        cat && cat.length > 0 && cat.forEach( geo => {
          if(geo.geotag && geo.geotag.latitude && geo.geotag.longitude){
            gt.push(geo);
          }
        });
      });
    } else{
      lodash.forEach(md[menu], (cat)=> {
          if(cat.category === category && cat.geotag && cat.geotag.latitude && cat.geotag.longitude){
            gt.push(cat);
          }
      });
    }

    this.setState({
      //dataSource: this.state.dataSource.cloneWithRows(this.props.model.business),
      loaded: true,
      geolocationData: gt ,
      allData:gt
    });
  }

  setSelectedOption(selectedOption){
    //console.log('selected option ' + selectedOption);
    this.setState({
      selectedOption
    });
    this.onPressCategory(selectedOption);
  }

  onPressCategory(selectedOption){
    let pins = [];
    if(selectedOption==='reset'){
      this.getMapData();
    }else{
       this.state.allData.filter((obj) =>{
        if(selectedOption === obj.category)
          pins.push(obj)
       });
    }
    this.setState({geolocationData:pins});
    this.closeControlPanel();
  }

  gotoDetails=(data)=>{
     this.props.pushNewRoute('detailView', {  data: data});
  }

  render() {
    const options = [
    "reset",
    "restaurant",
    "resort",
    "tour",
    "business"
    ];

    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>

        <Header>
          <HeaderContent/>
        </Header>

        <Content>
          <View style={[styles.container, {marginTop: 3}]}>

            <MapView style={styles.map}

              region={{
                latitude: this.state.me.latitude,
                longitude: this.state.me.longitude,
                latitudeDelta: 1.1,
                longitudeDelta: 1.0
              }}

               showsCompass={false}
               showsMyLocationButton={true}
               showsUserLocation={true}
               showsIndoors={false}
               toolbarEnabled={true}
               showsPointsOfInterest={true}
               showsBuildings={true}
               showsTraffic={false}
               rotateEnabled={false}
               pitchEnabled={false}
               loadingEnabled
               showsScale
               moveOnMarkerPress={false}
            >

              {this.state.geolocationData.length ? this.state.geolocationData.map( obj => (

                <MapView.Marker
                     identifier={obj.id}
                     coordinate={obj.geotag}
                     title={obj.title}
                     onCalloutPress={(e) => this.gotoDetails(obj)}
                     pinColor={PINCOLORS[obj.menu] || COLORS.TARSHARE_COLOR}
                   >

                </MapView.Marker>

              )) : null
            }
            </MapView>
          </View>

      </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
    return {
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops)),
    }
}

export default connect(null, bindAction)(nearby);
