'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Platform,
  Dimensions,
  WebView
} from 'react-native';

import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';
const COLORS = require('./../../constants/colors');
const CONFIGURE = require('./../../constants/configure');
import theme from '../../themes/base-theme';
//import { openDrawer } from '../../actions/drawer';
import SearchBar from 'react-native-searchbar';
import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Card, CardItem, Thumbnail } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';
import HeaderContent from './../headerContent/';
import { Spinner } from 'native-base';

// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

class gallery extends Component{

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
    }
  }

  render(){
    const jsCode = `
      document.querySelector('header').style.display = 'none';
      document.querySelector('footer').style.display = 'none'; 
      document.querySelector('.cookies-eu').style.display = 'none';
      document.querySelector('.main').style.paddingTop = '0px'; 
      document.querySelector('.main > article').style.paddingTop = '0px'; 
    `;
    const data = this.props.data;
    const url = (Platform.OS === 'ios') ?  {uri: CONFIGURE.IMAGE_PATH + data.category + '/' + data.pdf}:
           {uri: 'http://docs.google.com/gview?embedded=true&url=' + CONFIGURE.IMAGE_PATH + data.category + '/' + data.pdf};
    return (
      <Container theme={theme}>
        <Header>
            <HeaderContent/>
        </Header>

        <View style={{flex:1, height: height, width: width}}>

          <WebView
            style={{flex: 1, height: height -100, width: width, backgroundColor: '#F0F0F0'}}
            source={url}
            injectedJavaScript={jsCode}
            javaScriptEnabled={true}
            domStorageEnabled
            startInLoadingState={ (Platform.OS === 'ios') ? false : true }
            scalesPageToFit={true}
          />
      </View>
    </Container>
    );
   }

  renderError(){
    this.setState({isLoading: false});
    return(
      <View>
        <Text>
          No Internet connection
        </Text>
      </View>
    );
  }
}

function bindAction(dispatch) {
    return {
        //openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(gallery);
