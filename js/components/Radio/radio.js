import React from 'react';
import {
  View,
  WebView
} from 'react-native';

export default class Music extends React.Component{
  render(){
    return(
      <WebView
        source={{html: html}}
        javaScriptEnabled={true}
        domStorageEnabled={true}
      />
    );
  }
}

const html = `<html>
<head>
  <meta name="viewport" content="width=device-width">
  <meta name="referrer" content="origin">
</head>
<body>
  <video controls playsinline autoplay name="media" width="100%">
    <source src="http://live.kiss102fmbohol.com:8000/kiss" type="audio/mpeg">
  </video>
</body></html>`;
