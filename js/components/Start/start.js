'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ListView,
  Image,
  Platform,
  ScrollView,
  TouchableHighlight,
  BackAndroid,
  Alert
} from 'react-native';

import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';

import I18n from '../../../assets/data/translate.js';
import Accordion from 'react-native-accordion';
//import { openDrawer } from '../../actions/drawer';

var MenuData = require('../../../assets/data/menu.json');
var images = require('../../../assets/data/images');
const COLORS = require('./../../constants/colors');

import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Badge } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';
import theme from '../../themes/base-theme';
import SearchBar from 'react-native-searchbar';
import lodash from 'lodash';
const CONFIGURE = require('./../../constants/configure');
import Toast from 'react-native-root-toast';


class start extends Component{

  constructor(props){
    super(props);

    this.state = {
      model: [],
      searchModel:[],
      menuModel:[],
      searching: false,
      menuSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      }),

      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      })
    }
  }

  componentWillMount(){
    this.getData();
    if(!this.props.model.network){
      Toast.show(I18n.t('nointernetmessage'), {
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 100,
      });
    }
  }

  getData(){
    let k = lodash.concat(this.props.model.accomodation, this.props.model.dining, this.props.model.sites, this.props.model.culture, this.props.model.touring, this.props.model.shopping, this.props.model.bannking, this.props.model.public2 );

    this.setState({
      searchModel: k,
      dataSource: this.state.dataSource.cloneWithRows(MenuData),
    });
  }

  menuData=(data)=>{
    this.setState({
      menuModel: data,
      menuSource: this.state.dataSource.cloneWithRows(data),
    });
  }

  search=()=>{
    this.state.searching = !this.state.searching ;
    this.state.searching ? this.searchBar.show() : this.searchBar.hide();
  }

  _handleResults=(data)=>{
    if(!this.searchBar.getValue()){
      this.menuData([]);
    } else {
      this.menuData(data);
    }
  }

  _handleHide=(input)=>{
    this.menuData([]);
  }

  launchView(category, menu){

    if(category === 'facts'){
      this.gotoFacts();
      return;
    }

    if(category === 'fiestas'){
      this.gotoFiestas(this.props.model.culture, category);
      return;
    }

    if(category === 'phrases'){
      this.gotoPhrases(this.props.model.culture, category);
      return;
    }

    if(category === 'misssaulog' | category === 'candidates' || category === 'misspanglao' || category === 'girlscuaa' || category === 'boyscuaa' || category ==='mscampus' || category === 'mrcampus'){
      this.gotoVote(this.props.model[menu], category);
      return;
    }

    if(category === 'dyrdam' || category === 'dyrdfm'){
      this.gotoRadio(this.props.model[menu], category);
    }

    this.props.pushNewRoute('menu', {category: category, menu: menu, model: this.props.model, tracker: this.props.tracker });
  }

  gotoDetails=(data)=>{
    this.props.pushNewRoute('detailView', {  data: data, tracker: this.props.tracker });
  }

  gotoFacts=()=>{
    this.props.pushNewRoute('facts', { model: this.props.model });
  }

  gotoVote=(vote, category)=>{
    this.props.pushNewRoute('vote', { model: vote,  category: category });
  }

  gotoRadio=(vote, category)=>{
    this.props.pushNewRoute('radio', { model: this.props.model,  category: category });
  }

  gotoFiestas=(data)=>{
    var k = lodash.filter(this.props.model.culture, (obj)=>{
      return(obj.id === 'fiestas')
    });
    if(k && k.length > 0){
      this.props.pushNewRoute('detailView', { data: k[0]});
    }
  }

  gotoPhrases=(data)=>{
    var k = lodash.filter(this.props.model.culture, (obj)=>{
      return(obj.category === 'phrases')
    });
    if(k && k.length > 0){
      this.props.pushNewRoute('detailView', { data: k[0]});
    }
  }

  menuRow(row) {
    if(row.id === undefined){
      return <View/>
    }
    let source = row.banner ? { uri: CONFIGURE.IMAGE_PATH + row.category + '/' +row.banner} : images[row.menu] && images[row.menu][row.id] || undefined ;

    return (
      <TouchableHighlight
        onPress={() => this.gotoDetails(row)}
        underlayColor='#29D92E'>
        <View style={styles.listcontainer}>
          <Image
          source={ images.noimagebanner  }
          style={[styles.wholeimage]} >
            <Image
              source={source || images.noimagebanner  }
              style={[styles.wholeimage]} >
                <View style={styles.headlineContainer}>
                  <Text style={styles.postTitle3}>{row.title}</Text>
                </View>
              </Image>
          </Image>
        </View>
      </TouchableHighlight>
    );
  }

  renderRow(row) {
    let header = (
      <View style={styles.listcontainer}>
          <Image
            source={images.menu[row.id]}
            style={[styles.wholeimage]} >
              <View style={styles.headlineContainer}>
                <Text style={styles.postTitle3}>{I18n.t(row.id)}</Text>
                <View style={styles.redDot} />
              </View>
          </Image>
      </View>
    );

    let content = row.menus.map(obj => (

      <View>
        <TouchableHighlight
          underlayColor={COLORS.TARSHARE_COLOR}
          onPress={() => {this.launchView(obj, row.id)}}>
          <View style={styles.menuContainer}>
            <Text style={styles.eventTitle}>{I18n.t(obj)}</Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <Icon style={{color: COLORS.LINKS}} name={'ios-arrow-forward-outline'} />
            </View>
          </View>
        </TouchableHighlight>
      </View>

    ));

    return(
      <Accordion
        style={{backgroundColor: '#fff'}}
        expanded={row.id === 'public2' ? true : false}
        header={header}
        content={<View>{content}</View>}
        easing="linear"
      />
    );
  }
  _scrollToSection = (refAlias) => {
    const scrollviewRef = this.refs.wgw_scrollview
    var childHandle = findNodeHandle(this.refs[refAlias]) // this is the section you're scrolling to
    var scrollViewHandle = findNodeHandle(scrollviewRef) // this is the parent scrollview
    if (childHandle == null) {
      UIManager.measureLayout(
        childHandle, scrollViewHandle, (error) => {
          console.log(error)
        }, (relPosX, relPosY) => {
          scrollviewRef.scrollTo({
            x: 0,
            y: relPosY
          })
        }
      )
    }
  }
  gotoMap=()=>{
      this.props.pushNewRoute('nearby', { model: this.props.model });
  }

  render(){
    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>
        <Header>
          <View style={styles.header} >
    				<View style={styles.rowHeader}>
              <Button transparent onPress={this.search} style={{ width: 65, alignSelf: 'center'}}>
                <Icon name='ios-search-outline' color={'#fff'} style={{color: '#1C499E', fontSize: 30}} />
              </Button>

    					<Image source={require('../../../images/logo_small.png')} style={{flex: 1, height: 90, width: 90, resizeMode: 'contain', alignSelf: 'center'}}>
    					</Image>

              <Button transparent onPress={this.gotoMap} style={{ width: 65, alignSelf: 'center'}}>
                { (!CONFIGURE.HUAWEI) ?
                    <Icon name='ios-map-outline'  style={{color: '#1C499E', fontSize: 30} } /> :
                      <View/>
                }
              </Button>

    				</View>
    			</View>
        </Header>
        <ScrollView style={{marginTop: 5}}>
          {(this.state.menuModel && this.state.menuModel.length > 0) ?
            <ListView
              ref='_sv'
              dataSource={this.state.menuSource}
              renderRow={this.menuRow.bind(this)}
              style={styles.listView}
            />:
            <ListView
              style={{backgroundColor: '#fff'}}
              dataSource={this.state.dataSource}
              renderRow={this.renderRow.bind(this)}
            />
        }
        <SearchBar
          ref={(ref) => this.searchBar = ref}
          data={this.state.searchModel}
          placeholder={I18n.t('search')}
          handleResults={this._handleResults}
          iOSPadding={false}
          allDataOnEmptySearch={false}
          autoCorrect={false}
          clearOnHide={false}
          hideBack
        />
      </ScrollView>

    </Container>
    );
  }
}

function bindAction(dispatch) {
    return {
        //openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}

export default connect(null, bindAction)(start);
