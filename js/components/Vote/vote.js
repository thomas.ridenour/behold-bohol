'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ListView,
  Image,
  Platform,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  Alert,
  NetInfo
} from 'react-native';


// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

import styles from '../../styles/style.js';
import store  from 'react-native-simple-store';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

import lodash from 'lodash';
import I18n from '../../../assets/data/translate.js';

const COLORS = require('./../../constants/colors');
var images = require('../../../assets/data/images');
import theme from '../../themes/base-theme';
import SearchBar from 'react-native-searchbar';
import Swiper from 'react-native-swiper';

import { Container, Header, InputGroup, Input, Icon, Button, Content, Title, Card, CardItem, Thumbnail } from 'native-base';
import { connect} from 'react-redux';
import { pushNewRoute, replaceRoute } from '../../actions/route';
import HeaderContent from './../headerContent/';
import api from '../../api/tarshare.js';
const CONFIGURE = require('./../../constants/configure');
import Toast from 'react-native-root-toast';
import { Spinner } from 'native-base';


class vote extends Component{

  constructor(props){
    super(props);

    this.state = {
      isLoading: true,
      model: this.props.model,
      device: {},
      searching: false,
      candidates: {},
      loaded: false,
      dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
      })
    }
  }

  componentWillMount() {
    NetInfo.isConnected.addEventListener('change', Function.prototype);

    store.get('device').then((device) => {
			this.setState({device: device});
		});

    var k = lodash.filter(this.state.model, (obj)=>{
      return(obj.category === this.props.category)
    });

    this.getData(k);
  }

  componentDidMount(){
    this.timer = setTimeout(() => {
      this.setState({loaded: true});
    }, 1000);
  }

  updateVoteData=(data)=>{
    NetInfo.isConnected.fetch().then(connected => {
      this.setState({isConnected : connected});
      if(connected){
          api.getVotesCampaign(this.props.category).then(value => {
            this.setState({isLoading: false});

            let cand = this.state.candidates.slice();
            cand.forEach(obj => {
              var k = lodash.find(value.Items, lodash.matchesProperty('id', obj.id));
              if(k){
                obj.votes = k.votes;
              }
            });

            let nds = new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            });

            this.setState({
              candidates: cand,
              dataSource: nds.cloneWithRows(cand)
            });

          }).catch((error) => {
            console.warn('VVVV error ' + error);
          });
      } else {
        Toast.show('You need an internet connection to vote.', {
          duration: Toast.durations.SHORT,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 100,
        });
      }
    });
  }

  getData=(data)=>{
    if(data){
      var sorted = data.slice();
      this.setState({
        candidates: sorted,
        dataSource: this.state.dataSource.cloneWithRows(sorted)
      });

      this.updateVoteData(data);
    }
  }

  cityVote=()=>{
    Alert.alert(
      'Tagbilaran City',
      'Please download the new Tagbilaran City Application to Vote',
      [
        {text: I18n.t('ok'), onPress: () => console.log("viewed_welcome") },
      ],
      { cancelable: false }
    );
  }

  checkVote=(data)=>{
    if(this.state.isConnected){
        Alert.alert(
        I18n.t('votefor') + data.name + '?',
        '',
        [
          {text: I18n.t('yes'), onPress: () => this.recordVote(data) },
          {text: I18n.t('no'), onPress: () => console.log("viewed_welcome") },
        ],
        { cancelable: false }
      );
    } else {
      Alert.alert(
      'No Internet',
      'You need an internet connection to vote.',
      [
        {text: I18n.t('ok'), onPress: () => console.log("viewed_welcome") },
      ],
      { cancelable: false }
    );
    }
  }

  recordVote=(data)=>{
    data.deviceid = this.state.device.id;
    data.campaign = this.props.category;
    this.setState({isLoading: true});
    api.voteCampaign(data).then(value => {
      this.setState({isLoading: false});
      if(value.statusCode === '200'){
        var sorted = this.state.candidates.slice();

        var n = lodash.findIndex(sorted, lodash.matchesProperty('id', data.id));
        sorted[n].votes = value.votes;
        let nds = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });

        this.setState({
          candidates: sorted,
          dataSource: nds.cloneWithRows(sorted)
        });

        this.voted(value);

      } else {
        this.votedwarning(value);
      }
    });
  }

  votedwarning=(data)=>{
    Alert.alert(
      I18n.t('votewarningtitle'),
      data.err,
      [
        {text: I18n.t('ok'), onPress: () => console.log("viewed_welcome") }
      ],
      { cancelable: false }
    );
  }

  voted=(data)=>{
    Alert.alert(
      I18n.t('votetitle'),
      I18n.t('votebody'),
      [
        {text: I18n.t('ok'), onPress: () => console.log("viewed_welcome") }
      ],
      { cancelable: false }
    );
  }

  renderCard(event){
    return(
      <View>
          <Card style={{ flex: 0, margin: 10 }} >
                <View style={[styles.sectionEvent, {paddingHorizontal: 14, paddingVertical: 12}]}>
                  <View style={[ {flex:2}]}>
                    <Text style={[styles.sectionText, {color: 'black'}]}>
                      {event && event.name}
                    </Text>
                    <Text style={[styles.sectionText, {color: 'black', fontSize: 16, fontWeight: '500'}]}>
                      {event && event.title}
                    </Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.checkVote(event)}
                    >
                    <View style={[{marginRight: 0, flex: 1}]}>
                      <View style={{alignSelf: 'flex-end', display: 'none' }}>
                          <Icon name='ios-heart' style={{color: '#F65B3A', fontSize: 33}}/>
                      </View>

                      <Text style={[styles.sectionText, {color: 'black', fontSize: 10, fontWeight: '500', textAlign: 'right'}]}>
                      Votes {event && event.votes}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={{flex:1}}>

                    {event.images && event.images.map(obj => (
                      <View style={[styles.slide, {flex:1}]}>
                        <Image
                          style={[{height: width * (event.factor ? event.factor : 0.50), width: width - 40 , resizeMode: 'contain', flex: 1, alignSelf: 'center', marginBottom: 8}]}
                          source={images.noimage}
                        >
                            <Image
                              style={[{height: width * (event.factor ? event.factor : 0.50) , width: width - 30 , resizeMode: 'contain', flex: 1, alignSelf: 'center', marginBottom: 8}]}
                              source={{ uri: CONFIGURE.IMAGE_PATH + event.category + '/' + obj}}
                            />
                        </Image>
                      </View>
                    ))}
              </View>
                <View style={{paddingHorizontal: 14, paddingVertical: 10}}>
                  <Text style={{textAlign: 'justify'}}>{event.description}</Text>
              </View>
         </Card>

     </View>
    );
  }

  _handleHide=(input)=>{
    this.shapeData(this.state.model.events);
  }

  render(){
    return (
      <Container theme={theme} style={{backgroundColor: '#fff'}}>

        <Header>
            <HeaderContent/>
        </Header>

        <ScrollView style={{marginTop: 8}}>
          <Text style={{textAlign: 'center', paddingVertical: 12, backgroundColor: '#fff', color: '#F65B3A', fontSize: 20, fontWeight: '600'}}>
            Vote Everyday   <Icon name='ios-heart' style={{color: '#F65B3A', fontSize: 25,  backgroundColor: 'transparent'}}/>
          </Text>

          <Image
            style={[{ width: width - 100 , resizeMode: 'contain', flex: 1, alignSelf: 'center', marginBottom: 8}]}
            source={images.voteeveryday}
          />

          {this.state.isLoading &&
            <View style={{flex: 1, alignItems: 'flex-end', marginRight: 22, marginTop: -64}}>
              <Spinner color="#B1B2A6" size="small"/>
            </View>
          }

          {(this.state.loaded === false) &&
            <Spinner color="#B1B2A6" size="large"/>
          }

          {this.state.loaded &&
            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderCard.bind(this)}
              style={styles.listView}
            />
          }
        </ScrollView>

    </Container>
    );
   }
}

function bindAction(dispatch) {
    return {
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops))
    }
}
export default connect(null, bindAction)(vote);
