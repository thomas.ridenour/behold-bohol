'use strict';

import React, { Component } from 'react';
import { TouchableOpacity, Image, Platform } from 'react-native';
import { connect } from 'react-redux';
//import { openDrawer, closeDrawer } from '../../actions/drawer';
import { popRoute } from '../../actions/route';
import { Icon, View, Text, Button, InputGroup, Input, Title } from 'native-base';
import theme from '../../themes/base-theme';
import styles from './styles';
const COLORS = require('./../../constants/colors');
import I18n from '../../../assets/data/translate.js';


class HeaderContent extends Component {
	navigateTo(route) {
        //this.props.closeDrawer();
    }

    popRoute() {
        this.props.popRoute();
    }

	render() {
		let back =(Platform.OS === 'android') ? 'md-arrow-back' : 'ios-arrow-back-outline';
		return (

			<View style={styles.header} >
				<View style={styles.rowHeader}>
					<Button transparent onPress={() => this.popRoute()} style={{width:65, alignSelf: 'center'}}>
						<Icon name={back} style={{color: '#1C499E', fontSize: 30}}/>
					</Button>

					<Image source={require('../../../images/logo_small.png')} style={{flex: 1, height: 90, width: 90, resizeMode: 'contain', alignSelf: 'center', marginRight: 65}}>
					</Image>


				</View>
			</View>
		);
	}
}

function bindAction(dispatch) {
    return {
				//openDrawer: ()=>dispatch(openDrawer()),
				//closeDrawer: ()=>dispatch(closeDrawer()),
        popRoute:()=>dispatch(popRoute())
    }
}

export default connect(null, bindAction)(HeaderContent);
				//	<Text style={styles.headerText}>{I18n.t('appname')}</Text>
