'use strict';

var React = require('react-native');

var { StyleSheet, Dimensions, Platform } = React;

var primary = require('../../themes/variable').brandPrimary;


module.exports = StyleSheet.create({
	container: {
			flex: 1,
			width: null,
			height: null
	},
	header: {
		marginLeft: Platform.OS === 'android' ? -30 : 0,
		width: Dimensions.get('window').width,
		//paddingLeft: 15,
		//paddingRight: 15,
		backgroundColor: primary
	},
	rowHeader: {
		//flex: 1,
		flexDirection: 'row',
		// justifyContent: 'space-between',
		// alignSelf: 'stretch',
		paddingTop: Platform.OS === 'android' ? 7 : 0
	},
	btnHeader: {
		//paddingTop: 8,
		padding: 0,
		// paddingLeft: 0,
		color: '#1C499E'
	},
	imageHeader: {
		height: 25,
		width: 95,
		resizeMode: 'contain',
		marginTop: 10
	},
	headerText: {
		color: '#fff',
		alignSelf: 'center',
		fontSize: 20
	}
});
