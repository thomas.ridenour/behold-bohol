/* @flow */
'use strict';

import React from 'react';
import { ActivityIndicatorIOS, Platform } from 'react-native';
import ProgressBar from "ProgressBarAndroid";
import NativeBaseComponent from 'native-base/Components/Base/NativeBaseComponent';
import computeProps from 'native-base/Utils/computeProps';


export default class SpinnerNB extends NativeBaseComponent {

    prepareRootProps() {

        var type = {
            height: 50
        }

        var defaultProps = {
            style: type
        }

        return computeProps(this.props, defaultProps);

    }


    render() {
        return(
           <ProgressBar  {...this.prepareRootProps()} styleAttr = "Horizontal"
                                                    indeterminate = {true}
                                                    color={'#137C7A'}  />
        );
    }

}
