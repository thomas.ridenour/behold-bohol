/* @flow */
'use strict';

import React from 'react';
import { ActivityIndicatorIOS, Platform } from 'react-native';
import NativeBaseComponent from 'native-base/Components/Base/NativeBaseComponent';
import computeProps from 'native-base/Utils/computeProps';


export default class SpinnerNB extends NativeBaseComponent {

    prepareRootProps() {

        var type = {
            height: 80
        }

        var defaultProps = {
            style: type
        }

        return computeProps(this.props, defaultProps);

    }


    render() {
        return(
            <ActivityIndicatorIOS {...this.prepareRootProps()}    color={'#137C7A'}
                                                                size={this.props.size ? this.props.size : "large" } />
        );
    }

}
