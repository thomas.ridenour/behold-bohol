'use strict';

import React, { Component} from 'react';
import { Image, Platform, View, ScrollView, Linking, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import { closeDrawer } from '../../actions/drawer';
import { replaceOrPushRoute, resetRoute } from '../../actions/route';

import { Container, Content, Text, Icon, List, ListItem, Button } from 'native-base';

import styles from './style';
const COLORS = require('./../../constants/colors');

import DeviceInfo from 'react-native-device-info';
import I18n from '../../../assets/data/translate.js';
import store  from 'react-native-simple-store';
import lodash from 'lodash';
import api from '../../api/tarshare.js';
import { pushNewRoute, replaceRoute } from '../../actions/route';
const CONFIGURE = require('./../../constants/configure');
import Modal from 'react-native-simple-modal';


class SideBar extends Component {
	navigateTo(route) {
        this.props.closeDrawer();
        this.props.replaceOrPushRoute(route);
    }
    resetRoute(route) {
        this.props.closeDrawer();
        this.props.resetRoute(route);
    }

	constructor(props){

	    super(props);
	    this.state = {
				open: false,
	      favorites: undefined,
				user:{}

	    }
	  }

	componentWillMount(){
		let user = {
			name : DeviceInfo.getDeviceName(),
			brand : DeviceInfo.getBrand(),
			model: DeviceInfo.getModel(),
			country: DeviceInfo.getDeviceCountry(),
			locale: DeviceInfo.getDeviceLocale(),
			id: DeviceInfo.getUniqueID(),
			id2: DeviceInfo.getDeviceId(),
			build: DeviceInfo.getBuildNumber(),
			version: DeviceInfo.getVersion(),
			instanceId: DeviceInfo.getInstanceID()
		}
		store.save('device', user);

		this.setState({user: user});
	}

	componentDidMount(){
		debugger;
		this.fetchData();
	}

	fetchData=()=>{
		store.get('favorites').then((favorites) => {
			this.setState({favorites: favorites});
		});
	}

	launch=(url)=>{
    	if(url.includes('@')){
        	url = 'mailto:' + url;
		} else if(!url.includes('http') && !isNaN(+'+ 65 () 123'.replace(/\D/g,''))){
			url = 'tel:' + url.replace(/\D/g,'');
		}

	    Linking.canOpenURL(url).then(supported => {
	      if (!supported) {
	        console.log('******* Can\'t handle url: ' + url);
	      } else {
	        console.log('******* Opening: ' + url);
	        return Linking.openURL(url);
	      }
	    }).catch(err => console.error('******* An error occurred', err));
	  }

	launchAbout=()=>{
		this.props.closeDrawer();

		this.setState({open:true});
	}

	launchSurvey=()=>{
		//save user info?

		this.launch(CONFIGURE.SURVEY);

	}

	launchView=(data)=>{
		this.props.closeDrawer();
		this.props.pushNewRoute('detailView', {  data: data });
	}

    render(){
        return (
                <View  style={styles.background} >
											<Image
												style={styles.welcomeImage}
												source={require('../../../images/logo.jpg')}
											/>

											<List  foregroundColor={'#fff'}
												style={styles.section} >
													<ListItem
															 iconLeft
															style={styles.links}>

															<Text style={[styles.linkText, {paddingLeft:  8}]}>{I18n.t('links')}</Text>
													</ListItem>
											</List>

											<List foregroundColor={'black'}
													>
													<ListItem
															button iconLeft
															onPress={() => this.launch('https://www.facebook.com/Boholtourismph/')}
															style={[styles.links, {padding: 6, paddingLeft: 24, backgroundColor: COLORS.MENU_BLUE}]}
													>
															<Icon name='logo-facebook' style={{fontSize: 22}}/>
															<Text style={styles.linkText} >{I18n.t('tourismoffice')}</Text>
													</ListItem>

													<ListItem
															button iconLeft
															onPress={() => this.launch('lovetarshare@gmail.com')}
															style={[styles.links, {padding: 6, paddingLeft: 24, backgroundColor: COLORS.MENU_GREEN}]}
													>
															<Icon name='ios-mail-outline' style={{fontSize: 22}}/>
															<Text style={styles.linkText} >{I18n.t('contacttarshare')}</Text>
													</ListItem>

													<ListItem
															button iconLeft
															onPress={() => this.launchSurvey()}
															style={[styles.links, {padding: 6, paddingLeft: 24, backgroundColor: COLORS.MENU_YELLOW}]}
													>
															<Icon name='ios-happy-outline' style={{fontSize: 22}}/>
															<Text style={styles.linkText} >{I18n.t('survey')}</Text>
													</ListItem>
											</List>

											<Modal
												open={this.state.open}
												offset={0}
												overlayBackground={'rgba(0, 0, 0, 0.75)'}
												animationDuration={200}
												animationTension={40}
												modalDidOpen={() => undefined}
												modalDidClose={() => undefined}
												closeOnTouchOutside={true}
												containerStyle={{
												   justifyContent: 'center'
												}}
												modalStyle={{
												   borderRadius: 2,
												   margin: 20,
												   padding: 10,
												   backgroundColor: '#F5F5F5'
												}}>
											</Modal>

                </View>
        );
    }
}

function bindAction(dispatch) {
    return {
        closeDrawer: ()=>dispatch(closeDrawer()),
				pushNewRoute:(route, passprops)=>dispatch(pushNewRoute(route, passprops)),

        replaceOrPushRoute:(route)=>dispatch(replaceOrPushRoute(route))
    }
}

export default connect(null, bindAction)(SideBar);
