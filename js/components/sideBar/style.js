'use strict';

var React = require('react-native');

var { StyleSheet, Platform } = React;

var primary = require('../../themes/variable').brandPrimary;
const COLORS = require('./../../constants/colors');

let Dimensions = require('Dimensions');

// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

module.exports = StyleSheet.create({
    eventTitle:{
      color: COLORS.LIST_LABEL,
      fontSize: width/20,
      fontWeight: '600',
      //color: COLORS.INACTIVE_ICON
    },
    menuContainer:{
      flex: 1,
      flexDirection: 'row',
      padding: 8,
      paddingVertical: 6,
      borderWidth: 1,
      marginTop: -2,
      color: COLORS.TARSHARE_COLOR,
      borderBottomColor: COLORS.TARSHARE_COLOR,
      backgroundColor: '#fff',
      fontWeight: '500'
    },

    links: {
        //flex: 1,
        // paddingTop: Platform.OS === 'android' ? 4 : 5,
        // paddingBottom: Platform.OS === 'android' ? 4 : 5,
        // paddingLeft: Platform.OS === 'android' ? 0 : 10,
        borderBottomWidth: Platform.OS === 'android' ? 1 : 1,
        marginLeft: -12,
        borderBottomColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center'

    },
    section:{
      // borderBottomColor: '#fff',
      // borderTopColor: '#fff',
      // borderTopWidth: 2,
      // borderBottomWidth: 2,
      marginLeft: 0,
      backgroundColor: COLORS.TARSHARE_COLOR
    },
    linkText: {
        paddingLeft: 0,
        fontSize: 18,
        paddingBottom: 3
    },
    logoutContainer: {
        padding: 30
    },
    logoutbtn: {
        paddingTop: 30,
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: '#797979'
    },
    background: {
        flex: 1,
        width: null,
        height:null,
        backgroundColor: '#fff'
    },
    drawerContent: {
        //paddingTop: Platform.OS === 'android' ? 20 : 30
    },
    profilePic: {
        height: 40,
        width: 40,
        borderRadius: Platform.OS === 'android' ? 40 : 20
    },

    welcomeImage:{
      //flex: 1,
      resizeMode: 'contain',
      //alignSelf: 'flex-start',
      width: width * .80,
      height: (width * 0.5),
      overflow: 'hidden'
    },
    welcomeContainer:{
      //flex: 1,
      padding: 8,
      justifyContent: 'center'
    },

    welcomeText: {
      fontSize: 20,
      lineHeight: 26,
      justifyContent: 'center',
      alignSelf: 'center',
      alignItems: 'center',
      textAlign: 'center',
      color: COLORS.TARSHARE_COLOR
    },
});
