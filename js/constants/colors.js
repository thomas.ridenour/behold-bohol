export const INACTIVE_ICON = '#818286';
export const ACTIVE_ICON = '#179C99';
export const TARSHARE_COLOR = '#179C99';

//export const INACTIVE_ICON = '#818286';

//frogtail colors
export const LIST_LABEL = '#313131';
export const TOOLBAR_GRAY = '#F3F3F3';
export const FROG_GREEN = '#86ad32';
export const FROG_GREEN_BORDER = '#7fbd36';
export const FROG_GRAY = '#AAAAAA';
export const FROG_GRAY_BORDER = '#a8a8a8';

export const CONTROL_GRAY = '#EBEBEB';
export const TEXT_BLACK = '#060606';
export const TEXT_CONTROL = '#757575';
export const PANEL_GREEN = '#B4C48F';
export const PANEL_GRAY = '#C8CFBD';
export const TEXT_GREEN = '#51A600';
export const TEXT_LISTVIEW_GRAY = '#676767';
export const TEXT_LISTVIEW_ROW_GRAY = '#797979';
export const TEXT_LABEL = '#8D8D8D';
export const BORDER_GRAY = '#8C8C8C';
export const INPUT_BORDER_GRAY = '#C8CACE';
export const INVOICE_GRAY= '#3A3A3A';

//Revision 2
export const ROW_BORDER = '#D6D6D6';
export const CARD_BACK = '#303443';
export const CONTROL_BACKGROUND = '#EEF2F4';
export const LABEL_GRAY = '#8C8F92';
export const DATA_TEXT_GRAY = '#7A7A7A';
export const INSTRUCTION_TEXT_GRAY = '#626262';
export const ICON = '#C7C7CC';
export const GREEN_TEXT = '#74A95F';
export const RED_TEXT = '#D9594F';
export const LILLY_WHITE = '#EEF2F4';//'#EBEBEB';

//Menu COLORS
export const MENU_YELLOW = '#FEFCB2';
export const MENU_PURPLE = '#A5E3FF';
export const MENU_GREEN = '#B4FBAA';
export const MENU_ORANGE = '#E48844';
export const MENU_BLUE = '#A2FEFB';
export const MENU_NAVYBLUE = '#0E70D7';
export const MENU_PINK = '#FCA2C6';
export const MENU_RED = '#940C07';
export const MENU_GINGER = '#B66C00';
export const FACEBOOK = '#3B5998';
export const INSTAGRAM = '#262626';
export const YOUTUBE = '#CC181E';
export const LINKS = '#4C6875';
export const GOOGLEPLUS = '#DB4437';
export const TWITTER = '#009DF1';
export const BB_RED = '#EB4B2F';



//default apple colors
export const AZURE = '#439FDF';
export const MAYA_BLUE = '#5CC9FD';
export const SUPERNOVA = '#FFCA00';
export const UFO_GREEN = '#53D75A';
export const PIZAZZ = '#FE9500';
export const RED_ORANGE = '#FD402A';
export const RADICAL_RED = '#FD3554';
export const OSLO_GRAY = '#8E8E93';

export const getColor = function(index){
  //return '#fff';
  switch(index%4){
    case 0:
      return MENU_YELLOW;
    case 1:
      return MENU_PURPLE;
    case 2:
      return MENU_GREEN
    case 3:
      return MENU_BLUE;
  }
  return MAYA_BLUE;
}
