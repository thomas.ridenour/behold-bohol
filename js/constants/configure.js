export const SERVER = 'https://44prwh3d82.execute-api.ap-southeast-1.amazonaws.com/'
export const ENV =  'prod/tarshare/';
export const PATH = SERVER + ENV;
export const IMAGE_PATH = 'https://media.conceptmobile.net/bohol/';
export const PRODUCT = 'bohol';
export const SURVEY = 'https://docs.google.com/forms/d/e/1FAIpQLSdTHYCFvVeu3fLI6PmgPZJ2_qA8VWqhlZWua_sDYJ3aWFs33g/viewform';
export const HUAWEI = false;
