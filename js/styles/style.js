import React, { Component } from 'react';
import {
  StyleSheet,
  Platform,
  PixelRatio
} from 'react-native';

import Global from './global.js';
const COLORS = require('./../constants/colors');

let Dimensions = require('Dimensions');

// Get the width and height of the window
const {
  width,
  height
} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      flexDirection: 'column',
    },

  // LIST VIEW

  listcontainer: {
    marginBottom: Platform.OS === 'ios' ? 0 : -1,
    marginTop: Platform.OS === 'ios' ? 0 : 0,
  },

  //used by banner
  wholeimage: {
    resizeMode: 'contain',
    width: width,
    height: (width * 0.34)
  },

  wholeimage2: {
    resizeMode: 'contain',
    width: width - 20,
    height: (width * 0.3133),
    marginBottom: 0,
  },

  adimage: {

      resizeMode: 'contain'
  },

  redDot: {
    opacity: 0.8,
    height: 16,
    width: 16,
    borderRadius: 16,
    backgroundColor: 'red',
    alignSelf: 'center',
    position: 'absolute',
    right: 8,
    top: 8

  },

  headlineContainer : {
    flexDirection: 'row',
    width: width,
    backgroundColor: 'rgba(0,0,0,0.33)',
    position: 'absolute',
    bottom: 0,
    left: 0,
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 3,
    paddingBottom: 3,
  },

  menuContainerList: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: COLORS.LILLY_WHITE,
    marginHorizontal: 10,
    marginBottom: 20,
    borderColor: COLORS.LILLY_WHITE,
    borderWidth: (Platform.OS === 'ios') ? 2 : 2,
  },

  headlineContainer7 : {
    width: width,
    justifyContent: 'center',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 3,
    paddingBottom: 3,
  },

  headlineContainer4 : {
    backgroundColor: '#fff',
    padding: 8
  },

  postTitle: {
    fontSize: 19,
    fontWeight: '700',
    textAlign: 'center',
    color: '#1C499E',
  },

  postTitle2: {
    fontSize: 15,
    fontWeight: '700',
    color: '#fff',
  },

  postTitle3: {
    fontSize: 22,
    fontWeight: '700',
    color: '#fff',
  },

  postTitle8: {
    fontSize: 15,
    fontWeight: '400',
    marginBottom: 4,
    color: COLORS.INSTRUCTION_TEXT_GRAY,
  },

  postTitle7: {
    fontSize: 19,
    fontWeight: '600',
    paddingBottom: 4,
    color: COLORS.INSTRUCTION_TEXT_GRAY,
  },

  promoTitle: {
    fontSize: 17.25,
    fontWeight: '800',
    color: '#fff',
    textAlign: 'center'
  },
  postLocation: {
    fontSize: 14,
    fontWeight: '600',
    textAlign:'center',
    textAlign: 'center',
    color: '#1C499E',
  },
  description: {
    fontSize: 10,
    paddingLeft: 9,
    color: '#3e3e3e',
  },

   eventDetail:{
     padding: 10
  },

   eventTitle:{
     color: COLORS.LINKS,
     fontSize: 17.7,
     fontWeight: '500',

     //color: COLORS.INACTIVE_ICON
   },

   eventDescription:{
     color: COLORS.INSTRUCTION_TEXT_GRAY,
     fontSize: 14,
     fontWeight: '400',
     textAlign: 'justify'
   },

   eventLocation:{
     color: COLORS.INSTRUCTION_TEXT_GRAY,
     fontSize: 17.25,
     fontWeight: '600',
   },

   eventTime:{
     color: COLORS.INSTRUCTION_TEXT_GRAY,
     fontSize: 17.25,
     fontWeight: '600',
   },

   rowContainer:{
     //flex: 1,
     flexDirection: 'row',
     alignItems: 'center',
     alignSelf: 'center'
   },

   rowRight:{
     flex: 1,
     flexDirection: 'row',
     justifyContent: 'flex-end',
     padding: 0,
     //paddingRight: 12
   },
   eventRight:{
     padding: 0,
     alignItems: 'flex-end'
   },

  dateRowLabel:{
    fontSize: 29.571,
    fontWeight: '400',
  },

  mainRowLabel:{
    color: COLORS.INACTIVE_ICON,
    fontSize: 25.875,
    fontWeight: '800',
  },
  mainRowDescription:{
    color: COLORS.INACTIVE_ICON,
    fontSize: 15.92,
    fontWeight: '600',
  },


  logo: {
      width: 48,
      height: 48,
      resizeMode: 'contain',
      alignSelf: 'flex-end'
    },

    logo2: {
        width: 70,
        height: 70,
        paddingHorizontal: 8,
        resizeMode: 'contain',
        justifyContent: 'center',
        alignSelf: 'center'
      },

    //map
    map: {
      width:width,
      height:height-50,
    },

    staticmap:{
      backgroundColor: '#fff',
    },

    listtext:{
      fontSize: 16,
      color: COLORS.TEXT_LISTVIEW_GRAY
    },

    swiperText:{
      color: 'rgba(255,255,255,0.7)',
      fontSize: 65,
      height:65,
      fontWeight: 'bold',
    },

    //slider
    slide: {
      justifyContent: 'center',
      alignItems: 'center',
    },

    slideImage:{
      //width: width,
      //backgroundColor: '#fff',
      height: width * 0.6148,
      resizeMode: 'contain'
    },
    slidePromoContainer:{
      //flex: 1,
      //height: 90,
      //flexDirection: 'row'
    },
    slidePromo:{
      flex: 1,
      padding: 12,
      justifyContent: 'center',
      alignItems: 'center'
    },

    contactsContainer:{
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      padding: 12
    },

    icon:{
      height: 33,
      width: 33,
    },

    textButton:{
      fontSize: 16,
      textAlign: 'justify',
      color: COLORS.LINKS
    },

    iconText:{
      fontSize: 22,
      color: COLORS.TARSHARE_COLOR,
    },

    menuContainer:{
      flex: 1,
      flexDirection: 'row',
      paddingHorizontal: 8,
      paddingVertical: 8,
      alignItems: 'center',
      borderWidth: 1,
      borderColor: COLORS.LILLY_WHITE,
      backgroundColor: '#fff',
      fontWeight: '500'
    },

    sectionContainer:{
      backgroundColor: '#fff',
      flexDirection: 'row',
      alignItems: 'center',
      padding: 3,
      paddingLeft: 11
    },

    sectionEvent:{
      flexDirection: 'row',
      alignItems: 'center',
    },

    sectionText:{
      color: COLORS.INSTRUCTION_TEXT_GRAY,
      fontSize: 18,
      fontWeight: '700',
    },
    hoursTitle:{
      fontSize: 16,
      color: COLORS.INSTRUCTION_TEXT_GRAY,

    },
    hoursText: {
      fontSize: 16,
      paddingLeft: 8,
      fontWeight: '600',
      color: COLORS.INSTRUCTION_TEXT_GRAY
    },

    eventsTitleText:{
      fontSize: 12,
      fontWeight: '600',
      color: '#fff',

    },
    hoursContainer: {
      padding: 8,
      flexDirection: 'row'
    },
    addressContainer:{
      flex: 1,
      padding: 8,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },

    addressContainer2:{
      flex: 1,
      padding: 8,
      flexDirection: 'row',
      justifyContent: 'center'
    },

    featureContainer:{
      padding: 8,
    },
    titleContainer:{
      paddingTop: 15,
      paddingBottom: 12,
    },
      menusegmentedcontrol:{
        width: width * .95,
        alignSelf: 'center',
        marginTop: 12,
        marginBottom: 12
      },
      header: {
    		marginLeft: Platform.OS === 'android' ? -30 : 0,
    		width: Dimensions.get('window').width,
    	},
    	rowHeader: {
    		flex: 1,
    		flexDirection: 'row',
    		// justifyContent: 'space-between',
    		// alignSelf: 'stretch',
    		paddingTop: 0, //Platform.OS === 'android' ? 0 : 0,
        alignItems: 'center'
    	},
    	btnHeader: {
    		//paddingTop: 8,
    		padding: 0,
    		// paddingLeft: 0,
    		color: '#fff'
    	},
    	imageHeader: {
    		height: 25,
    		width: 95,
    		resizeMode: 'contain',
    		marginTop: Platform.OS === 'ios' ? 10 : 0
    	},
    	headerText: {
    		color: '#fff',
    		alignSelf: 'center',
    		fontSize: 20
    	},

      emojiButtons: {
        fontSize: 25,
        textAlign: 'center'
      }
});

export default styles;
