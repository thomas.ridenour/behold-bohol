import React, { Component } from 'react';
import {
  StyleSheet
} from 'react-native';

const Welcome = StyleSheet.create({

  container: {
    padding: 30,
    paddingTop: 50,
    flex: 1,
    backgroundColor:'#007966',
  },
	bar: {
    marginTop: 30,
    marginBottom: 10,
  },

});


export default Welcome;
